<?php

namespace App\Domain\User\Transaction;

class TransactionManager
{
    /**
     * @var TransactionFactory
     */
    private $factory;

    public function __construct (
        TransactionFactory $factory
    ) {
        $this->factory = $factory;
    }

    public function create(string $txId, string $eth, string $gzr, \DateTime $confirmedAt): Transaction
    {
        $social = $this->factory->create();
        $social->setTxId($txId);
        $social->setEth($eth);
        $social->setGzr($gzr);
        $social->setConfirmedAt($confirmedAt);

        return $social;
    }
}