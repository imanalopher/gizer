<?php

namespace App\Domain\User\Transaction;

use App\Document\Transaction;

class TransactionFactory
{
    public function create() : Transaction
    {
        return new Transaction();
    }
}