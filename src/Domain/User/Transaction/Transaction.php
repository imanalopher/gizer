<?php

namespace App\Domain\User\Transaction;

class Transaction
{
    /**
     * @var string
     */
    protected $txId;

    /**
     * @var int
     */
    protected $eth;

    /**
     * @var int
     */
    protected $gzr;

    /**
     * @var \DateTime
     */
    protected $confirmedAt;

    /**
     * @return string
     */
    public function getTxId():? string
    {
        return $this->txId;
    }

    /**
     * @param string $txId
     */
    public function setTxId(string $txId): void
    {
        $this->txId = $txId;
    }

    /**
     * @return float
     */
    public function getEth():? float
    {
        return $this->eth;
    }

    /**
     * @param float $eth
     */
    public function setEth(float $eth): void
    {
        $this->eth = $eth;
    }

    /**
     * @return float
     */
    public function getGzr():? float
    {
        return $this->gzr;
    }

    /**
     * @param float $gzr
     */
    public function setGzr(float $gzr): void
    {
        $this->gzr = $gzr;
    }

    /**
     * @return \DateTime
     */
    public function getConfirmedAt():? \DateTime
    {
        return $this->confirmedAt;
    }

    /**
     * @param \DateTime $confirmedAt
     */
    public function setConfirmedAt(\DateTime $confirmedAt): void
    {
        $this->confirmedAt = $confirmedAt;
    }
}