<?php

namespace App\Domain\User\Social;

class SocialManager
{
    /**
     * @var SocialFactory
     */
    private $factory;

    public function __construct (
        SocialFactory $factory
    ) {
        $this->factory = $factory;
    }

    public function create(string $name, string $url): Social
    {
        $social = $this->factory->create();
        $social->setName($name);
        $social->setUrl($url);

        return $social;
    }
}