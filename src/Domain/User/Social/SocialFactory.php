<?php

namespace App\Domain\User\Social;

use App\Document\Social;

class SocialFactory
{
    public function create() : Social
    {
        return new Social();
    }
}