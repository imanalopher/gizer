<?php

namespace App\Domain\User\Gzr;

class GzrManager
{
    /**
     * @var GzrFactory
     */
    private $factory;

    public function __construct(
        GzrFactory $factory
    ) {
        $this->factory = $factory;
    }

    public function create(string $type, string $id, int $amount): Gzr
    {
        $gzr = $this->factory->create();
        $gzr->setType($type);
        $gzr->setId($id);
        $gzr->setAmount($amount);

        return $gzr;
    }
}