<?php

namespace App\Domain\User\Gzr;

use App\Document\Gzr;

class GzrFactory
{
    public function create() : Gzr
    {
        return new Gzr();
    }
}