<?php

namespace App\Domain\User\Avatar;

use App\Document\Avatar;

class AvatarFactory
{
    public function create() : Avatar
    {
        return new Avatar();
    }
}