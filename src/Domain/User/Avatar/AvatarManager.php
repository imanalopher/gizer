<?php

namespace App\Domain\User\Avatar;

class AvatarManager
{
    /**
     * @var AvatarFactory
     */
    private $factory;

    public function __construct(
        AvatarFactory $factory
    ) {
        $this->factory = $factory;
    }

    public function create(string $type, string $uri): Avatar
    {
        $avatar = $this->factory->create();
        $avatar->setType($type);
        $avatar->setUri($uri);

        return $avatar;
    }
}