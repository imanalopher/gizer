<?php

namespace App\Domain\User;

use App\Document\Gzr;
use App\Document\User;

class UserFactory
{
    public function create() : User
    {
        $user = new User();
        $user->setGzr(new Gzr());

        return $user;
    }
}