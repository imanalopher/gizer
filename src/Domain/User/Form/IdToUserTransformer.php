<?php

namespace App\Domain\User\Form;

use App\Document\User;
use App\Domain\User\UserRepositoryInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class IdToUserTransformer implements DataTransformerInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $repository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Transforms an object (User) to a int (id).
     *
     * @param  User|null $item
     * @return int
     */
    public function transform($item)
    {
        if (null === $item) {
            return '';
        }

        if ($item instanceof User) {
            return $item->getId();
        }

        return $item;
    }

    /**
     * Transforms a int (id) to an object (User).
     *
     * @param  int $id
     * @return Resource|null
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($id)
    {
        // no resource url? It's optional, so that's ok
        if (!$id) {
            return null;
        }

        $resource = $this->repository->find($id);

        if (null === $resource) {

            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'An user with id "%s" does not exist!',
                $id
            ));
        }

        return $resource;
    }
}