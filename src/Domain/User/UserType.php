<?php

namespace App\Domain\User;

use App\Domain\User\Address\AddressType;
use App\Domain\User\Avatar\AvatarType;
use App\Domain\User\Gzr\GzrType;
use App\Domain\User\Social\SocialType;
use App\Domain\User\Transaction\TransactionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nick')
            ->add('email')
            ->add('type')
            ->add('username')
            ->add('password')
            ->add('active')
            ->add('about')
            ->add('reputation')
            ->add('timezone')
            ->add('interests', CollectionType::class, [
                'entry_type' => TextType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('boosts')
            ->add('address', AddressType::class)
            ->add('gzr', GzrType::class)
            ->add('avatar', AvatarType::class)
            ->add('social', CollectionType::class, [
                'entry_type' => SocialType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('knows', CollectionType::class, [
                'entry_type' => TextType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('owns', CollectionType::class, [
                'entry_type' => TextType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('transactions', CollectionType::class, [
                'entry_type' => TransactionType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => \App\Document\User::class
        ]);
    }
}