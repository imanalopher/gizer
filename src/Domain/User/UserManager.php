<?php

namespace App\Domain\User;

use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserManager
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var UserFactory
     */
    private $factory;

    public function __construct(
        UserRepository $userRepository,
        UserPasswordEncoderInterface $passwordEncoder,
        UserFactory $factory
    ) {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->factory = $factory;
    }

    public function create(string $username, string $email, string $password)
    {
        $user = $this->factory->create();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->getGzr()->setId(uniqid());

        $password = $this->encodePassword($password, $user);

        $user->setPassword($password);

        $this->userRepository->save($user);

        return $user;
    }

    public function save(\App\Document\User $user)
    {
        $this->userRepository->save($user);
    }

    /**
     * @param string $password
     * @param $user
     * @return string
     */
    public function encodePassword(string $password, $user): string
    {
        return $this->passwordEncoder->encodePassword($user, $password);
    }

    /**
     * @param User $user
     * @return null|User|object
     */
    public function isUnique(User $user):? User
    {
        return $this->userRepository->findOneBy(['gzr.id' => $user->getGzr()->getId()]);
    }

}