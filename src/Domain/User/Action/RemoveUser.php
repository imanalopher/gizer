<?php

namespace App\Domain\User\Action;

use App\Domain\Item\Item;
use App\Domain\User\UserRepositoryInterface;
use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class RemoveUser
{
    use ControllerTrait;
    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(
        RequestStack $request,
        FormFactoryInterface $formFactory,
        UserRepositoryInterface $userRepository
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->userRepository = $userRepository;
    }

    public function __invoke()
    {
        $id = $this->request->getCurrentRequest()->get('id');
        /** @var Item $item */
        $item = $this->userRepository->find($id);

        $this->userRepository->remove($item);

        $view = $this->view(null, 201);

        return $this->handleView(
            $view
        );
    }

}