<?php

namespace App\Domain\User\Action;

use App\Domain\User\UserRepositoryInterface;
use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GetUser
{
    use ControllerTrait;
    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(
        RequestStack $request,
        FormFactoryInterface $formFactory,
        UserRepositoryInterface $userRepository
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->userRepository = $userRepository;
    }

    public function __invoke()
    {
        $id = $this->request->getCurrentRequest()->get('id');

        if (!$user = $this->userRepository->find($id)) {
            throw new NotFoundHttpException('There are no user with that id');
        }

        $view = $this->view($user, 200);
        $view->getContext()->addGroup('get_users');

        return $this->handleView(
            $view
        );
    }

}