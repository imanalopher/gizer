<?php

namespace App\Domain\User\Action;

use App\Domain\User\Model\Criteria;
use App\Domain\User\UserRepositoryInterface;
use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class GetUsers
{
    use ControllerTrait;
    
    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(
        RequestStack $request,
        FormFactoryInterface $formFactory,
        UserRepositoryInterface $userRepository
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->userRepository = $userRepository;
    }

    public function __invoke(Criteria $criteria)
    {
        $users = $this->userRepository->findAllByCriteria($criteria);

        $view = $this->view($users, 200);
        $view->getContext()->addGroup('get_users');

        return $this->handleView(
            $view
        );
    }

}