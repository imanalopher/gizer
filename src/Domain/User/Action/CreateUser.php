<?php

namespace App\Domain\User\Action;

use App\Domain\SendGrid\Call\Lists;
use App\Domain\SendGrid\Call\SignUpConfirmationMail;
use App\Domain\SendGrid\Call\Recipients;
use App\Domain\User\User;
use App\Domain\User\UserManager;
use App\Domain\User\UserRepositoryInterface;
use App\Domain\User\UserType;
use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CreateUser
{
    use ControllerTrait;

    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var UserManager
     */
    private $manager;

    /**
     * @var Recipients
     */
    private $recipients;

    /**
     * @var Lists
     */
    private $lists;

    /**
     * @var int
     */
    private $listId;

    /**
     * @var SignUpConfirmationMail
     */
    private $signUpConfirmationMail;

    public function __construct(
        RequestStack $request,
        FormFactoryInterface $formFactory,
        UserRepositoryInterface $userRepository,
        UserManager $manager,
        Recipients $recipients,
        Lists $lists,
        $listId,
        SignUpConfirmationMail $signUpConfirmationMail
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->userRepository = $userRepository;
        $this->manager = $manager;
        $this->recipients = $recipients;
        $this->lists = $lists;
        $this->listId = $listId;
        $this->signUpConfirmationMail = $signUpConfirmationMail;
    }

    public function __invoke()
    {
        $form = $this->formFactory->create(UserType::class);

        $data = $this->request->getCurrentRequest()->request->all();
        $form->submit($data);

        /** @var User $user */
        $user = $form->getData();

        if ($this->manager->isUnique($user)) {
            throw new BadRequestHttpException(
                sprintf('User with wallet ID %s is already registered', $user->getGzr()->getId())
            );
        }

        $this->preSave($user);

        $this->userRepository->save($user);

        $this->postSave($user);

        return $this->handleView($this->view(null, 201));
    }

    private function preSave(User $user)
    {
        if ($user->getPlainPassword()) {
            $user->setPassword($this->manager->encodePassword($user->getPlainPassword(), $user));
        }
    }

    private function postSave(User $user)
    {
        //SendGrid API calls
        $result = $this->recipients->addRecipients($user->getEmail(), $user->getNick());

        $recipientId = $result->persisted_recipients[0];
        $listId = $this->listId;

        $this->lists->addASingleRecipientToAList($listId, $recipientId);

        $this->signUpConfirmationMail->send($user->getEmail(), $user->getNick());
    }

}