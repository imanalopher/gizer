<?php

namespace App\Domain\User\Action;

use App\Domain\SendGrid\Call\TransactionConfirmationMail;
use App\Domain\User\Transaction\Transaction;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;
use App\Domain\User\UserType;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UpdateUser
{
    use ControllerTrait;
    
    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var TransactionConfirmationMail
     */
    private $transactionConfirmationMail;

    public function __construct(
        RequestStack $request,
        FormFactoryInterface $formFactory,
        UserRepositoryInterface $userRepository,
        TransactionConfirmationMail $transactionConfirmationMail
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->userRepository = $userRepository;
        $this->transactionConfirmationMail = $transactionConfirmationMail;
    }

    public function __invoke()
    {
        $id = $this->request->getCurrentRequest()->get('id');

        /** @var User $user */
        if (!$user = $this->userRepository->find($id)) {
            throw new NotFoundHttpException('There are no user with that id');
        }

        $transactions = new ArrayCollection();

        foreach ($user->getTransactions() as $transaction) {
            $transactions->add($transaction);
        }

        $user->clearTransactions();

        $form = $this->formFactory->create(UserType::class, $user);

        $data = $this->request->getCurrentRequest()->request->all();

        $form->submit($data, false);

        $user = $form->getData();

        foreach ($user->getTransactions() as $transaction) {
            $this->transactionConfirmationMail->send($user->getEmail(), $user->getNick(), $transaction->getGzr(), $transaction->getTxId());
        }

        /** @var Transaction $transaction */
        foreach ($transactions as $transaction) {
            $user->addTransaction($transaction);
        }

        $this->userRepository->save($user);

        $view = $this->view(null, 201);

        return $this->handleView($view);
    }

}