<?php

namespace App\Domain\User\Action;

use App\Document\Transaction;
use App\Domain\User\Transaction\TransactionType;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;
use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CreateUserTransaction
{
    use ControllerTrait;
    
    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(
        RequestStack $request,
        FormFactoryInterface $formFactory,
        UserRepositoryInterface $userRepository
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->userRepository = $userRepository;
    }

    public function __invoke()
    {
        $id = $this->request->getCurrentRequest()->get('id');

        /** @var User $user */
        if (!$user = $this->userRepository->find($id)) {
            throw new NotFoundHttpException('There are no user with that id');
        }

        $form = $this->formFactory->create(TransactionType::class);

        $data = $this->request->getCurrentRequest()->request->all();

        $form->submit($data, false);

        /** @var Transaction $transaction */
        $transaction = $form->getData();

        $user->addTransaction($transaction);

        $this->userRepository->save($user);

        $view = $this->view(null, 201);

        return $this->handleView($view);
    }

}