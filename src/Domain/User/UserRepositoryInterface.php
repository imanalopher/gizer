<?php

namespace App\Domain\User;

interface UserRepositoryInterface
{
    public function save(User $item);
    public function find($id, $lockMode = \Doctrine\ODM\MongoDB\LockMode::NONE, $lockVersion = NULL);
    public function findAll();
    public function remove(User $item);
    public function findAllByCriteria(Model\Criteria $criteria);
}