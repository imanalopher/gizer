<?php

namespace App\Domain\User;

use App\Domain\User\Address\Address;
use App\Domain\User\Gzr\Gzr;
use App\Domain\User\Avatar\Avatar;
use App\Domain\User\Social\Social;
use App\Domain\User\Transaction\Transaction;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

class User implements AdvancedUserInterface, \Serializable
{
    /** @var string */
    protected $nick;

    /** @var string */
    protected $email;

    /** @var string */
    protected $type;

    /** @var string */
    protected $username;

    /** @var string */
    protected $password;

    /** @var int */
    protected $active;

    /** @var string */
    protected $about;

    /** @var int */
    protected $reputation;

    /** @var string */
    protected $timezone;

    /** @var array */
    protected $interests;

    /** @var int */
    protected $boosts;

    /** @var Address */
    protected $address;

    /** @var Gzr */
    protected $gzr;

    /** @var Avatar */
    protected $avatar;

    /** @var Social */
    protected $social;

    /** @var array */
    protected $knows;

    /** @var array */
    protected $owns;

    /** @var string */
    protected $plainPassword;

    /** @var Transaction */
    protected $transactions;

    /** @var \DateTime */
    protected $createdAt;

    public function __construct()
    {
        $this->active = 1;
        $this->interests = [];
        $this->knows = [];
        $this->owns = [];
        $this->social = new ArrayCollection();
        $this->transactions = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    /**
     * @inheritdoc
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @inheritdoc
     */
    public function getSalt(): ? string
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getPassword(): ? string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @inheritdoc
     */
    public function getRoles(): array
    {
        return array('ROLE_USER');
    }

    /**
     * @inheritdoc
     */
    public function eraseCredentials(): void
    {
    }

    /**
     * @inheritdoc
     * @see \Serializable::serialize()
     */
    public function serialize(): ? string
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->active
        ));
    }

    /**
     * @inheritdoc
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized): void
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->active
            ) = unserialize($serialized);
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function isActive(): ? int
    {
        return $this->active;
    }

    public function setActive(?int $active): void
    {
        $this->active = $active;
    }

    /**
     * @inheritdoc
     */
    public function isAccountNonExpired(): bool
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function isAccountNonLocked(): bool
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function isCredentialsNonExpired(): bool
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function isEnabled(): ? int
    {
        return $this->active;
    }

    /**
     * @return string
     */
    public function getNick(): ?string
    {
        return $this->nick;
    }

    /**
     * @param string $nick
     */
    public function setNick(?string $nick): void
    {
        $this->nick = $nick;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getAbout(): ?string
    {
        return $this->about;
    }

    /**
     * @param string $about
     */
    public function setAbout(?string $about): void
    {
        $this->about = $about;
    }

    /**
     * @return int
     */
    public function getReputation(): ?int
    {
        return $this->reputation;
    }

    /**
     * @param int $reputation
     */
    public function setReputation(?int $reputation): void
    {
        $this->reputation = $reputation;
    }

    /**
     * @return string
     */
    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    /**
     * @param string $timezone
     */
    public function setTimezone(?string $timezone): void
    {
        $this->timezone = $timezone;
    }

    /**
     * @return array
     */
    public function getInterests(): array
    {
        return $this->interests;
    }

    /**
     * @param array $interests
     */
    public function setInterests(?array $interests): void
    {
        $this->interests = $interests;
    }

    /**
     * @return int
     */
    public function getBoosts(): ?int
    {
        return $this->boosts;
    }

    /**
     * @param int $boosts
     */
    public function setBoosts(?int $boosts): void
    {
        $this->boosts = $boosts;
    }

    /**
     * @return Address
     */
    public function getAddress(): ?Address
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress(?Address $address): void
    {
        $this->address = $address;
    }

    /**
     * @return Gzr
     */
    public function getGzr(): ?Gzr
    {
        return $this->gzr;
    }

    /**
     * @param Gzr $gzr
     */
    public function setGzr(Gzr $gzr): void
    {
        $this->gzr = $gzr;
    }

    /**
     * @return Avatar
     */
    public function getAvatar(): ?Avatar
    {
        return $this->avatar;
    }

    /**
     * @param Avatar $avatar
     */
    public function setAvatar(?Avatar $avatar): void
    {
        $this->avatar = $avatar;
    }

    /**
     * @return Collection|null
     */
    public function getSocial(): ? Collection
    {
        return $this->social;
    }

    /**
     * @param Social $social
     */
    public function addSocial(?Social $social): void
    {
        if (!$this->social->contains($social)) {
            $this->social->add($social);
        }
    }

    /**
     * @param Social $social
     */
    public function removeSocial(Social $social): void
    {
        if ($this->social->contains($social)) {
            $this->social->removeElement($social);
        }
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions():? Collection
    {
        return $this->transactions;
    }

    /**
     * @param Transaction $transaction
     */
    public function addTransaction(Transaction $transaction): void
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions->add($transaction);
        }
    }

    /**
     * @param Transaction $transaction
     */
    public function removeTransaction(Transaction $transaction): void
    {
        if ($this->transactions->contains($transaction)) {
            $this->transactions->removeElement($transaction);
        }
    }

    /**
     * @return array
     */
    public function getKnows(): array
    {
        return $this->knows;
    }

    /**
     * @param array $knows
     */
    public function setKnows(?array $knows): void
    {
        $this->knows = $knows;
    }

    /**
     * @return array
     */
    public function getOwns(): array
    {
        return $this->owns;
    }

    /**
     * @param array $owns
     */
    public function setOwns(?array $owns): void
    {
        $this->owns = $owns;
    }

    /**
     * @return string
     */
    public function getPlainPassword():? string
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     */
    public function setPlainPassword(string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt():? \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function clearTransactions()
    {
        $this->transactions = new ArrayCollection();
    }
}