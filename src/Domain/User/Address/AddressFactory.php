<?php

namespace App\Domain\User\Address;

use App\Document\Address;

class AddressFactory
{
    public function create() : Address
    {
        return new Address();
    }
}