<?php

namespace App\Domain\User\Address;

class AddressManager
{
    /**
     * @var AddressFactory
     */
    private $factory;

    public function __construct(
        AddressFactory $factory
    ) {
        $this->factory = $factory;
    }

    public function create(string $street, string $city, string $country): Address
    {
        $address = $this->factory->create();
        $address->setStreet($street);
        $address->setCity($city);
        $address->setCountry($country);

        return $address;
    }
}