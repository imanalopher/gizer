<?php

namespace App\Domain\Chest;

use App\Domain\Item\Form\ItemIdType;
use App\Domain\User\Form\UserIdType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                $chest = $event->getData();
                $form = $event->getForm();

                if ($chest->getId()) {

                    $form->add('status');
                    $form->add('user', UserIdType::class);
                    $form->add('items', CollectionType::class, [
                        'entry_type' => ItemIdType::class,
                        'allow_add' => true,
                        'allow_delete' => true,
                        'by_reference' => false
                    ]);

                } else {
                    $form->add('name');
                    $form->add('collection');
                    $form->add('price');
                }

                $form->add('transactionId');
            })
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => \App\Document\Chest::class
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}