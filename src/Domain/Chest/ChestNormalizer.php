<?php

namespace App\Domain\Chest;

use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class ChestNormalizer extends GetSetMethodNormalizer
{
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = array())
    {
        /** @var $object Chest */
        $object->setItems($object->getItems()->map(function ($item) {
            return $item->getId();
        }));

        return parent::normalize($object, $format, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Chest;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {

    }
}
