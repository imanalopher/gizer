<?php

namespace App\Domain\Chest\Model;

class Criteria
{
    /**
     * @var integer
     */
    protected $limit;

    /**
     * @var integer
     */
    protected $page;

    /**
     * @var string
     */
    protected $q;

    /**
     * @var array
     */
    protected $list;

    /**
     * Criteria constructor.
     * @param int $limit
     * @param int $page
     * @param string $q
     * @param array $list
     */
    public function __construct(?int $limit, ?int $page, ?string $q, ?array $list)
    {
        $this->limit = $limit;
        $this->page = $page;
        $this->q = $q;
        $this->list = $list;
    }

    /**
     * @return int
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getPage(): ?int
    {
        return $this->page;
    }

    /**
     * @return string
     */
    public function getQ(): ?string
    {
        return $this->q;
    }

    /**
     * @return array
     */
    public function getList(): ?array
    {
        return $this->list;
    }
}