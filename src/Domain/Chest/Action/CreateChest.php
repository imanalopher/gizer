<?php

namespace App\Domain\Chest\Action;

use App\Document\Chest;
use App\Domain\Chest\ChestManager;
use App\Domain\Chest\ChestType;
use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;

class CreateChest
{
    use ControllerTrait;

    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var ChestManager
     */
    private $chestManager;

    public function __construct(
        RequestStack $request,
        FormFactory $formFactory,
        ChestManager $chestManager
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->chestManager = $chestManager;
    }

    public function __invoke()
    {
        $form = $this->formFactory->create(ChestType::class, new Chest());

        $data = $this->request->getCurrentRequest()->request->all();
        $form->submit($data);

        $chest = $form->getData();

        $this->chestManager->save($chest);

        $view = $this->view($chest, 201);

        $view->getContext()->addGroup('created_chest');

        return $this->handleView(
            $view
        );
    }

}