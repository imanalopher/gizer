<?php

namespace App\Domain\Chest\Action;

use App\Document\Chest;
use App\Domain\Chest\ChestManager;
use App\Domain\Chest\ChestRepositoryInterface;
use App\Domain\Chest\ChestType;
use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Workflow\Registry;

class UpdateChestStatus
{
    use ControllerTrait;
    
    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var ChestManager
     */
    private $chestManager;
    
    /**
     * @var ChestRepositoryInterface
     */
    private $chestRepository;

    public function __construct(
        RequestStack $request,
        FormFactory $formFactory,
        ChestManager $chestManager,
        ChestRepositoryInterface $chestRepository
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->chestManager = $chestManager;
        $this->chestRepository = $chestRepository;
    }
    
    public function __invoke(Registry $registry)
    {
        $request = $this->request->getCurrentRequest();

        /** @var $chest Chest */
        if (!$chest = $this->chestRepository->find($request->get('id'))) {
            throw new NotFoundHttpException('Chest not found');
        }

        $form = $this->formFactory->create(ChestType::class, $chest);

        $data = $this->request->getCurrentRequest()->request->all();
        $form->submit($data, false);

        $chest = $form->getData();

        $stateMachine = $registry->get($chest, 'chest');
        $stateMachine->apply($chest, $chest->getStatus());

        $this->chestRepository->save($chest);

        $view = $this->view(null, 201);

        $view->getContext()->addGroup('get_chest');

        return $this->handleView(
            $view
        );
        
    }
}