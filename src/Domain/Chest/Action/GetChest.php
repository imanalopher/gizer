<?php

namespace App\Domain\Chest\Action;

use App\Domain\Chest\ChestManager;
use App\Domain\Chest\ChestRepositoryInterface;
use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;

class GetChest
{
    use ControllerTrait;
    
    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var ChestManager
     */
    private $chestManager;

    /**
     * @var ChestRepositoryInterface
     */
    private $chestRepository;

    public function __construct(
        RequestStack $request,
        FormFactory $formFactory,
        ChestManager $chestManager,
        ChestRepositoryInterface $chestRepository
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->chestManager = $chestManager;
        $this->chestRepository = $chestRepository;
    }
    
    public function __invoke()
    {
        $id = $this->request->getCurrentRequest()->get('id');
        $chest = $this->chestRepository->find($id);

        $view = $this->view($chest, 200);

        $view->getContext()->addGroups(['get_chest', 'get_chest_transaction']);

        return $this->handleView(
            $view
        );
        
    }
}