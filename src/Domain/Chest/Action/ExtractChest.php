<?php

namespace App\Domain\Chest\Action;

use App\Domain\Chest\ChestManager;
use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExtractChest
{
    use ControllerTrait;
    
    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var ChestManager
     */
    private $chestManager;

    public function __construct(
        RequestStack $request,
        FormFactory $formFactory,
        ChestManager $chestManager
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->chestManager = $chestManager;
    }
    
    public function __invoke()
    {
        if (!$chest = $this->chestManager->extract()) {
            throw new NotFoundHttpException('There are no chests available');
        }

        $view = $this->view($chest, 200);

        $view->getContext()->addGroup('extract_chest');

        return $this->handleView(
            $view
        );
        
    }
}