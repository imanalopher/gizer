<?php

namespace App\Domain\Chest\Action;

use App\Domain\Chest\ChestManager;
use App\Domain\Chest\ChestRepositoryInterface;
use App\Domain\Chest\Model\Criteria;
use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;

class GetChests
{
    use ControllerTrait;
    
    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var ChestManager
     */
    private $chestManager;

    /**
     * @var ChestRepositoryInterface
     */
    private $chestRepository;

    public function __construct(
        RequestStack $request,
        FormFactory $formFactory,
        ChestManager $chestManager,
        ChestRepositoryInterface $chestRepository
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->chestManager = $chestManager;
        $this->chestRepository = $chestRepository;
    }
    
    public function __invoke(Criteria $criteria)
    {
        $chests = $this->chestRepository->findAllByCriteria($criteria);

        $view = $this->view($chests, 200);

        $view->getContext()->addGroup('get_chest');

        return $this->handleView(
            $view
        );
        
    }
}