<?php

namespace App\Domain\Chest\Listener;

use App\Domain\Item\Item;
use App\Domain\Item\ItemRepositoryInterface;
use Symfony\Component\Workflow\Event\GuardEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UnlockedListener implements EventSubscriberInterface
{
    /**
     * @var ItemRepositoryInterface
     */
    private $itemRepository;

    public function __construct(ItemRepositoryInterface $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    public function unlocked(GuardEvent $event)
    {
        /** @var \App\Document\Chest $chest */
        $chest = $event->getSubject();

        foreach ($chest->getItems() as $item) {
            /** @var Item $item */
            $item = $this->itemRepository->find($item->getId());
            $item->unlock();

            $this->itemRepository->save($item);
        }

    }

    public static function getSubscribedEvents()
    {
        return array(
            'workflow.chest.guard.unlocked' => array('unlocked'),
        );
    }
}
