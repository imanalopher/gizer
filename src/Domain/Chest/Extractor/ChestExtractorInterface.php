<?php

namespace App\Domain\Chest\Extractor;

use App\Domain\Chest\Chest;

interface ChestExtractorInterface
{
    public function extract() : ?Chest;

}