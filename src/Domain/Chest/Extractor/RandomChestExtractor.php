<?php

namespace App\Domain\Chest\Extractor;

use App\Domain\Chest\Chest;
use App\Domain\Chest\ChestRepositoryInterface;

class RandomChestExtractor implements ChestExtractorInterface
{
    /**
     * @var ChestRepositoryInterface
     */
    private $chestRepository;

    public function __construct(
        ChestRepositoryInterface $chestRepository
    ) {
        $this->chestRepository = $chestRepository;
    }

    public function extract(): ?Chest
    {
        return $this->chestRepository->getRandom();
    }
}