<?php

namespace App\Domain\Chest;

use App\Domain\Chest\Extractor\ChestExtractorInterface;

class ChestManager
{
    /**
     * @var ChestExtractorInterface
     */
    private $extractor;

    /**
     * @var ChestRepositoryInterface
     */
    private $repository;

    /**
     * @var ChestFactory
     */
    private $factory;

    public function __construct(
        ChestExtractorInterface $extractor,
        ChestRepositoryInterface $chestRepository,
        ChestFactory $factory
    ) {
        $this->extractor = $extractor;
        $this->repository = $chestRepository;
        $this->factory = $factory;
    }

    public function extract() : ?Chest
    {
        return $this->extractor->extract();
    }

    public function save(Chest $chest)
    {
        $this->repository->save($chest);
    }

    public function create(): Chest
    {
        $item = $this->factory->create();

        $this->repository->save($item);

        return $item;
    }

}