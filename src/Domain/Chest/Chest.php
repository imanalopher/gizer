<?php

namespace App\Domain\Chest;

use App\Domain\Item\Item;
use App\Domain\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Workflow\MarkingStore\SingleStateMarkingStore;

class Chest extends SingleStateMarkingStore
{
    const STATUS_LOCKED = 'locked';
    const STATUS_PENDING = 'pending';
    const STATUS_UNLOCKED = 'unlocked';

    /** @var string */
    protected $name;

    /** @var string */
    protected $collection;

    /** @var int */
    protected $price;

    /** @var string */
    protected $status;

    /** @var User */
    protected $user;

    /** @var Collection|Item[] */
    protected $items;

    /** @var string */
    protected $transactionId;

    /** @var \DateTime */
    protected $createdAt;

    /**
     * Chest constructor.
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->status = self::STATUS_LOCKED;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getCollection(): ?string
    {
        return $this->collection;
    }

    /**
     * @param string $collection
     */
    public function setCollection(string $collection): void
    {
        $this->collection = $collection;
    }

    /**
     * @return int
     */
    public function getPrice(): ?int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return User
     */
    public function getUser():? string
    {
        return $this->user ? $this->user->getId() : null;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    /**
     * @param Item $item
     */
    public function addItem(Item $item): void
    {
        if (!$this->items->contains($item)) {
            $this->items->add($item);
        }
    }

    /**
     * @param Item $item
     */
    public function removeItem(Item $item): void
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
        }
    }

    /**
     * @param Item[]|Collection $items
     */
    public function setItems($items): void
    {
        $this->items = $items;
    }

    /**
     * @return string
     */
    public function getTransactionId():? string
    {
        return $this->transactionId;
    }

    /**
     * @param string $transactionId
     */
    public function setTransactionId(string $transactionId): void
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt():? \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}
