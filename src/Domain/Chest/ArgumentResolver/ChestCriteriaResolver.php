<?php

namespace App\Domain\Chest\ArgumentResolver;

use App\Domain\Chest\Model\Criteria;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ChestCriteriaResolver implements ArgumentValueResolverInterface
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return Criteria::class === $argument->getType();
    }

    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $criteria = new Criteria(
            $request->query->get('limit', 10),
            $request->query->get('page', 1),
            $request->query->get('q', null),
            $request->query->get('list', [])
        );

        yield $criteria;
    }
}