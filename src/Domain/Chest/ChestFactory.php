<?php

namespace App\Domain\Chest;

use App\Document\Chest;

class ChestFactory
{
    public function create() : Chest
    {
        return new Chest();
    }
}