<?php

namespace App\Domain\Chest;

interface ChestRepositoryInterface
{
    public function save(Chest $chest);
    public function remove(Chest $chest);
    public function getRandom();
    public function find($id, $lockMode = \Doctrine\ODM\MongoDB\LockMode::NONE, $lockVersion = NULL);
    public function findAllByCriteria(Model\Criteria $criteria);
}