<?php

namespace App\Domain\SendGrid;

interface RequestBuilderInterface
{
    /**
     * @return \Psr\Http\Message\RequestInterface
     */
    public function getRequest();
}