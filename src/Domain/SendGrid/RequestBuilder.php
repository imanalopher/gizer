<?php

namespace App\Domain\SendGrid;

use Http\Message\MessageFactory;

class RequestBuilder implements RequestBuilderInterface
{
    const METHOD_POST = 'POST';
    const ENDPOINT = 'https://api.sendgrid.com/v3';

    /**
     * @var MessageFactory
     */
    private $messageFactory;

    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var null|string
     */
    private $body = null;

    /**
     * @var string
     */
    private $apiKey;

    public function __construct(
        MessageFactory $messageFactory,
        string $apiKey
    ) {
        $this->messageFactory = $messageFactory;
        $this->apiKey = $apiKey;
    }

    public function setBody(?string $body)
    {
        $this->body = $body;
    }

    public function setMethod(string $method)
    {
        $this->method = $method;
    }

    public function setEndpoint(string $endpoint)
    {
        $this->endpoint = sprintf('%s%s', self::ENDPOINT, $endpoint);
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest()
    {
        return $this->messageFactory->createRequest(
            $this->method,
            $this->endpoint,
            [
                'Authorization' => sprintf('Bearer %s', $this->apiKey),
                'Content-Type' => 'application/json'
            ],
            $this->body
        );
    }
}
