<?php

namespace App\Domain\SendGrid;

use Psr\Http\Message\RequestInterface;

interface RequestSenderInterface
{
    /**
     * @param RequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function send(RequestInterface $request);
}