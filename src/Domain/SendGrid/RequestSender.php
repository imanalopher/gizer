<?php

namespace App\Domain\SendGrid;

use Http\Client\HttpClient;
use Psr\Http\Message\RequestInterface;

class RequestSender implements RequestSenderInterface
{
    /**
     * @var HttpClient
     */
    protected $client;

    public function __construct(HttpClient $client)
    {
        $this->client = $client;
    }

    /**
     * {@inheritdoc}
     */
    public function send(RequestInterface $request)
    {
        $response = $this->client->sendRequest($request);

        return json_decode($response->getBody()->getContents());
    }
}