<?php

namespace App\Domain\SendGrid\Call;

use App\Domain\SendGrid\RequestBuilder;

class TransactionConfirmationMail extends Call
{
    /**
     * @var RequestBuilder
     */
    protected $requestBuilder;

    public function send($email, $nick, $amount, $txId)
    {
        $this->requestBuilder->setMethod(RequestBuilder::METHOD_POST);
        $this->requestBuilder->setEndpoint('/mail/send');

        $this->requestBuilder->setBody(
            json_encode(
                [
                    "personalizations" => [
                        [
                            "to" => [
                                ["email" => $email, "name" => $nick]
                            ],
                            "subject" => sprintf("Thank you for your purchase %s.", $nick),
                            "substitutions" => [
                                "%nick%" => $nick,
                                "%amount%" => (string) $amount,
                                "%txid%" => $txId
                            ]
                        ]
                    ],
                    "from" => [
                        "email" => "receipts@gizer.io"
                    ],
                    "tracking_settings" => [
                        "click_tracking" => ["enable" => true],
                        "open_tracking" => ["enable" => true],
                        "ganalytics" => ["enable" => true]
                    ],
                    "template_id" => "bfef8196-308e-45e4-a8a9-fd47741902a7"
                ]
            )
        );

        return $this->execute();
    }

}