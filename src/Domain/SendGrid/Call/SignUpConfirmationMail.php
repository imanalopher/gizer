<?php

namespace App\Domain\SendGrid\Call;

use App\Domain\SendGrid\RequestBuilder;

class SignUpConfirmationMail extends Call
{
    /**
     * @var RequestBuilder
     */
    protected $requestBuilder;

    public function send($email, $firstName)
    {
        $this->requestBuilder->setMethod(RequestBuilder::METHOD_POST);
        $this->requestBuilder->setEndpoint('/mail/send');

        $this->requestBuilder->setBody(
            json_encode(
                [
                    "personalizations" => [
                        [
                            "to" => [
                                ["email" => $email, "name" => $firstName]
                            ],
                            "subject" => sprintf("Here's your whitelist confirmation, %s.", $firstName),
                            "custom_args" => [
                                "first_name" => $firstName
                            ]
                        ]
                    ],
                    "from" => [
                        "email" => "receipts@gizer.io"
                    ],
                    "tracking_settings" => [
                        "click_tracking" => ["enable" => true],
                        "open_tracking" => ["enable" => true],
                        "ganalytics" => ["enable" => true]
                    ],
                    "template_id" => "8117e3fe-642a-484b-b96c-9e15d372d66f"
                ]
            )
        );

        return $this->execute();
    }

}