<?php

namespace App\Domain\SendGrid\Call;

use App\Domain\SendGrid\RequestBuilderInterface;
use App\Domain\SendGrid\RequestSenderInterface;

abstract class Call
{
    /**
     * @var RequestBuilderInterface
     */
    protected $requestBuilder;

    /**
     * @var RequestSenderInterface
     */
    private $requestSender;

    public function __construct(
        RequestBuilderInterface $requestBuilder,
        RequestSenderInterface $requestSender
    ) {
        $this->requestBuilder = $requestBuilder;
        $this->requestSender = $requestSender;
    }

    protected function execute()
    {
        $request = $this->requestBuilder->getRequest();

        return $this->requestSender->send($request);
    }
}