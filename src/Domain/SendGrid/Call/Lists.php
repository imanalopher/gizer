<?php

namespace App\Domain\SendGrid\Call;

use App\Domain\SendGrid\RequestBuilder;

class Lists extends Call
{
    /**
     * @var RequestBuilder
     */
    protected $requestBuilder;

    public function addASingleRecipientToAList($listId, $recipientId)
    {
        $this->requestBuilder->setMethod(RequestBuilder::METHOD_POST);
        $this->requestBuilder->setEndpoint(sprintf('/contactdb/lists/%s/recipients/%s', $listId, $recipientId));
        $this->requestBuilder->setBody('');

        return $this->execute();
    }
}