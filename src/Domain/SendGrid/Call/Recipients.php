<?php

namespace App\Domain\SendGrid\Call;

use App\Domain\SendGrid\RequestBuilder;

class Recipients extends Call
{
    /**
     * @var RequestBuilder
     */
    protected $requestBuilder;

    public function addRecipients($email, $firstName)
    {
        $this->requestBuilder->setMethod(RequestBuilder::METHOD_POST);
        $this->requestBuilder->setEndpoint('/contactdb/recipients');

        $this->requestBuilder->setBody(
            json_encode([
                [
                    'email' => $email,
                    'first_name' => $firstName
                ]
            ])
        );

        return $this->execute();
    }
}