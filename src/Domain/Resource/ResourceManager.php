<?php

namespace App\Domain\Resource;

class ResourceManager
{
    /**
     * @var ResourceRepositoryInterface
     */
    private $repository;

    /**
     * @var ResourceFactory
     */
    private $factory;

    public function __construct(
        ResourceRepositoryInterface $repository,
        ResourceFactory $factory
    ) {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    public function save(Resource $resource)
    {
        $this->repository->save($resource);

        return $resource;
    }

    public function create(string $source)
    {
        $resource = $this->factory->create();
        $resource->getFile($source)->setSrc($source);

        $this->repository->save($resource);

        return $resource;
    }
}