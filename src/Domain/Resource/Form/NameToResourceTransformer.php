<?php

namespace App\Domain\Resource\Form;

use App\Domain\Resource\ResourceRepositoryInterface;
use App\Domain\Resource\Resource;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class NameToResourceTransformer implements DataTransformerInterface
{
    /**
     * @var ResourceRepositoryInterface
     */
    private $resourceRepository;

    public function __construct(ResourceRepositoryInterface $resourceRepository)
    {
        $this->resourceRepository = $resourceRepository;
    }

    /**
     * Transforms an object (resource) to a string (source).
     *
     * @param  Resource|null $resource
     * @return string
     */
    public function transform($resource)
    {
        if (null === $resource) {
            return '';
        }

        if ($resource instanceof Resource) {
            return $resource->getFile()->getSrc();
        }

        return $resource;
    }

    /**
     * Transforms a string (source) to an object (Resource).
     *
     * @param  string $name
     * @return Resource|null
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($name)
    {
        // no resource url? It's optional, so that's ok
        if (!$name) {
            return null;
        }

        $resource = $this->resourceRepository->findOneBy(['file.src' => $name]);

        if (null === $resource) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'An recource with url "%s" does not exist!',
                $name
            ));
        }

        return $resource;
    }
}