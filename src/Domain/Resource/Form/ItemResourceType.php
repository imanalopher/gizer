<?php

namespace App\Domain\Resource\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemResourceType extends AbstractType
{
    /**
     * @var NameToResourceTransformer
     */
    private $transformer;

    public function __construct(NameToResourceTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer($this->transformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'invalid_message' => 'The selected resource does not exist',
        ));
    }

    public function getParent()
    {
        return TextType::class;
    }
}