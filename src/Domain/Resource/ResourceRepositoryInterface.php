<?php

namespace App\Domain\Resource;

interface ResourceRepositoryInterface
{
    public function save(Resource $resource);
    public function remove(Resource $resource);
    public function findOneBy(array $criteria);
    public function findAllByCriteria(Model\Criteria $criteria);
}