<?php

namespace App\Domain\Resource\Action;

use App\Domain\Resource\Model\Criteria;
use App\Domain\Resource\ResourceRepositoryInterface;
use Doctrine\MongoDB\EagerCursor;
use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;

class GetResource
{
    use ControllerTrait;

    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var ResourceRepositoryInterface
     */
    private $repository;

    public function __construct(
        RequestStack $request,
        FormFactory $formFactory,
        ResourceRepositoryInterface $repository
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->repository = $repository;
    }

    public function __invoke(Criteria $criteria)
    {
        /** @var EagerCursor $resources */
        $resources = $this->repository->findAllByCriteria($criteria);

        $resources = array_map(function ($resource){
            return $resource->getSrc();
        }, $resources->toArray());

        $view = $this->view($resources, 200);

        return $this->handleView(
            $view
        );
    }

}