<?php

namespace App\Domain\Resource\Action;

use App\Domain\Resource\ResourceRepositoryInterface;
use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\HttpFoundation\RequestStack;
use App\Domain\Resource\ResourceType;
use Symfony\Component\Form\FormFactory;

class UploadResource
{
    use ControllerTrait;

    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var ResourceRepositoryInterface
     */
    private $repository;

    public function __construct(
        RequestStack $request,
        FormFactory $formFactory,
        ResourceRepositoryInterface $repository
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->repository = $repository;
    }

    public function __invoke()
    {
        $form = $this->formFactory->create(ResourceType::class);
        $form->handleRequest($this->request->getMasterRequest());

        $resource = $form->getData();

        $this->repository->save($resource);

        $view = $this->view($resource, 200);
        $view->getContext()->addGroup('get_resource');

        return $this->handleView($view);
    }

}