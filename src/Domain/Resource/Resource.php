<?php

namespace App\Domain\Resource;

use Symfony\Component\HttpFoundation\File\File;
use App\Document\File as EmbeddedFile;

class Resource implements \JsonSerializable
{
    /**
     * @var File
     */
    protected $imageFile;

    /**
     * @var EmbeddedFile
     */
    protected $file;

    /**
     * @var \DateTime
     */
    protected $updatedAt;

    public function __construct()
    {
        $this->file = new EmbeddedFile();
        $this->updatedAt = new \DateTimeImmutable();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getFile()->getSrc();
    }

    public function setImageFile(?File $image = null)
    {
        $this->imageFile = $image;

        if (null !== $image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setFile(EmbeddedFile $file)
    {
        $this->file = $file;
    }

    public function getFile(): EmbeddedFile
    {
        return $this->file;
    }

    public function getSrc()
    {
        return $this->file->getSrc();
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->getSrc();
    }
}
