<?php

namespace App\Domain\Resource;

use App\Document\Resource;

class ResourceFactory
{
    public function create(): Resource
    {
        return new Resource();
    }
}