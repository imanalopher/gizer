<?php

declare(strict_types=1);

namespace App\Domain\Item;

class Item
{
    /**
     * @var Resources
     */
    protected $resources;

    /**
     * @var Current
     */
    protected $current;

    /**
     * @var Meta
     */
    protected $meta;

    /**
     * @return Resources
     */
    public function getResources()
    {
        return $this->resources;
    }

    /**
     * @param Resources $resources
     */
    public function setResources(Resources $resources): void
    {
        $this->resources = $resources;
    }

    /**
     * @return Current
     */
    public function getCurrent():? Current
    {
        return $this->current;
    }

    /**
     * @param Current $current
     */
    public function setCurrent(Current $current): void
    {
        $this->current = $current;
    }

    /**
     * @return Meta
     */
    public function getMeta():? Meta
    {
        return $this->meta;
    }

    /**
     * @param Meta $meta
     */
    public function setMeta(Meta $meta): void
    {
        $this->meta = $meta;
    }

    public function unlock()
    {
        $this->meta->setUnlocked(new \DateTime());
        $this->current->setStatus(Current::STATUS_UNLOCKED);
    }
}
