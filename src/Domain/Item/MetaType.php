<?php

namespace App\Domain\Item;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MetaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('description', TextType::class)
            ->add('wallet', TextType::class)
            ->add('use', TextType::class)
            ->add('category', TextType::class)
            ->add('rarity', TextType::class)
            ->add('origin', TextType::class)
            ->add('collection', TextType::class)
            ->add('series', SeriesType::class)
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => \App\Document\Meta::class
        ]);
    }
}