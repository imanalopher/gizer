<?php

namespace App\Domain\Item;

use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class CurrentNormalizer extends GetSetMethodNormalizer
{
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = array())
    {
        /** @var $object Current */
        $object->setSimilar($object->getSimilar()->map(function ($item) {
            return $item->getId();
        }));

        return parent::normalize($object, $format, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Current;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {

    }
}
