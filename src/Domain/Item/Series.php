<?php

namespace App\Domain\Item;

class Series
{
    /**
     * @var int
     */
    protected $item;

    /**
     * @var int
     */
    protected $of;

    /**
     * @return int
     */
    public function getItem():? int
    {
        return $this->item;
    }

    /**
     * @param int $item
     */
    public function setItem(int $item): void
    {
        $this->item = $item;
    }

    /**
     * @return int
     */
    public function getOf():? int
    {
        return $this->of;
    }

    /**
     * @param int $of
     */
    public function setOf(int $of): void
    {
        $this->of = $of;
    }

}