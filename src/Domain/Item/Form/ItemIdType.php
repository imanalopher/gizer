<?php

namespace App\Domain\Item\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemIdType extends AbstractType
{
    /**
     * @var IdToItemTransformer
     */
    private $transformer;

    public function __construct(IdToItemTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer($this->transformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'invalid_message' => 'The selected item does not exist',
        ]);
    }

    public function getParent()
    {
        return TextType::class;
    }
}