<?php

namespace App\Domain\Item\Form;

use App\Document\Item;
use App\Domain\Item\ItemRepositoryInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class IdToItemTransformer implements DataTransformerInterface
{
    /**
     * @var ItemRepositoryInterface
     */
    private $repository;

    public function __construct(ItemRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Transforms an object (Item) to a int (id).
     *
     * @param  Item|null $item
     * @return int
     */
    public function transform($item)
    {
        if (null === $item) {
            return '';
        }

        if ($item instanceof Item) {
            return $item->getId();
        }

        return $item;
    }

    /**
     * Transforms a int (id) to an object (Item).
     *
     * @param  int $id
     * @return Resource|null
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($id)
    {
        // no resource url? It's optional, so that's ok
        if (!$id) {
            return null;
        }

        $resource = $this->repository->find($id);

        if (null === $resource) {

            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'An item with id "%s" does not exist!',
                $id
            ));
        }

        return $resource;
    }
}