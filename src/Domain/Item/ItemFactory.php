<?php

namespace App\Domain\Item;

use App\Document\Item;

class ItemFactory
{
    public function create() : Item
    {
        return new Item();
    }
}