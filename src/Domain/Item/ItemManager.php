<?php

namespace App\Domain\Item;

class ItemManager
{
    /**
     * @var ItemRepositoryInterface
     */
    private $repository;

    /**
     * @var ItemFactory
     */
    private $factory;

    public function __construct(
        ItemRepositoryInterface $repository,
        ItemFactory $factory
    ) {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    public function create(): Item
    {
        $item = $this->factory->create();

        $resources = new \App\Document\Resources();
        $resources->setModel(new \App\Document\Model());
        $item->setResources($resources);

        $item->setCurrent(new \App\Document\Current());

        $meta = new \App\Document\Meta();
        $meta->setSeries(new \App\Document\Series());
        $item->setMeta($meta);

        $this->repository->save($item);

        return $item;
    }

    public function save(Item $item)
    {
        $this->repository->save($item);
    }

}