<?php

namespace App\Domain\Item;

use App\Domain\Resource\Resource;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

class Resources
{
    /**
     * @var Resource
     */
    protected $icons;

    /**
     * @var Resource
     */
    protected $images;

    /**
     * @var array
     */
    protected $videos;

    /**
     * @var Item
     */
    protected $item;

    /**
     * @var Model
     */
    protected $model;

    /**
     * Resource constructor.
     */
    public function __construct()
    {
        $this->icons = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->videos = [];
    }

    /**
     * @return Item
     */
    public function getItem():? Item
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem(Item $item): void
    {
        $this->item = $item;
    }

    /**
     * @return Collection
     */
    public function getIcons(): Collection
    {
        return $this->icons;
    }

    /**
     * @param Resource $icon
     */
    public function addIcon(Resource $icon): void
    {
        if (!$this->icons->contains($icon)) {
            $this->icons->add($icon);
        }
    }

    /**
     * @param Resource $icon
     */
    public function removeIcon($icon): void
    {
        if ($this->icons->contains($icon)) {
            $this->icons->removeElement($icon);
        }
    }

    /**
     * @return Collection
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    /**
     * @param Resource $image
     */
    public function addImage(Resource $image): void
    {
        if (!$this->images->contains($image)) {
            $this->images->add($image);
        }
    }

    /**
     * @param Resource $image
     */
    public function removeImage($image): void
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
        }
    }

    /**
     * @return array
     */
    public function getVideos(): array
    {
        return $this->videos;
    }

    /**
     * @param array $videos
     */
    public function setVideos(array $videos): void
    {
        $this->videos = $videos;
    }

    /**
     * @return Model
     */
    public function getModel():? Model
    {
        return $this->model;
    }

    /**
     * @param Model $model
     */
    public function setModel(Model $model): void
    {
        $this->model = $model;
    }
}