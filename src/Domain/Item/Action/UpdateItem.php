<?php

namespace App\Domain\Item\Action;

use App\Domain\Item\ItemRepositoryInterface;
use App\Domain\Item\ItemType;
use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;

class UpdateItem
{
    use ControllerTrait;
    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var ItemRepositoryInterface
     */
    private $itemRepository;

    public function __construct(
        RequestStack $request,
        FormFactory $formFactory,
        ItemRepositoryInterface $itemRepository
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->itemRepository = $itemRepository;
    }

    public function __invoke()
    {
        $id = $this->request->getCurrentRequest()->get('id');
        $item = $this->itemRepository->find($id);

        $form = $this->formFactory->create(ItemType::class, $item);

        $data = $this->request->getCurrentRequest()->request->all();
        $form->submit($data, false);

        $item = $form->getData();

        $this->itemRepository->save($item);

        $view = $this->view(null, 201);

        return $this->handleView($view);
    }

}