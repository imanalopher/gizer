<?php

namespace App\Domain\Item\Action;

use App\Domain\Item\Item;
use App\Domain\Item\ItemRepositoryInterface;

use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;

class RemoveItem
{
    use ControllerTrait;
    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var ItemRepositoryInterface
     */
    private $itemRepository;

    public function __construct(
        RequestStack $request,
        FormFactory $formFactory,
        ItemRepositoryInterface $itemRepository
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->itemRepository = $itemRepository;
    }

    public function __invoke()
    {
        $id = $this->request->getCurrentRequest()->get('id');
        /** @var Item $item */
        $item = $this->itemRepository->find($id);

        $this->itemRepository->remove($item);

        $view = $this->view(null, 201);

        return $this->handleView(
            $view
        );
    }

}