<?php

namespace App\Domain\Item\Action;

use App\Domain\Item\ItemRepositoryInterface;

use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;

class GetItem
{
    use ControllerTrait;
    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var ItemRepositoryInterface
     */
    private $itemRepository;

    public function __construct(
        RequestStack $request,
        FormFactory $formFactory,
        ItemRepositoryInterface $itemRepository
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->itemRepository = $itemRepository;
    }

    public function __invoke()
    {
        $id = $this->request->getCurrentRequest()->get('id');
        $item = $this->itemRepository->find($id);

        $view = $this->view($item, 200);
        $view->getContext()->addGroup('get_items');

        return $this->handleView(
            $view
        );
    }

}