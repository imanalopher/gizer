<?php

namespace App\Domain\Item\Action;

use App\Domain\Item\Model\Criteria;
use App\Domain\Item\ItemRepositoryInterface;
use FOS\RestBundle\Controller\ControllerTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;

class GetItems
{
    use ControllerTrait;
    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var ItemRepositoryInterface
     */
    private $itemRepository;

    public function __construct(
        RequestStack $request,
        FormFactory $formFactory,
        ItemRepositoryInterface $itemRepository
    ) {
        $this->request = $request;
        $this->formFactory = $formFactory;
        $this->itemRepository = $itemRepository;
    }

    public function __invoke(Criteria $criteria)
    {
        $items = $this->itemRepository->findAllByCriteria($criteria);

        $view = $this->view($items, 200);
        $view->getContext()->addGroup('get_items');

        return $this->handleView(
            $view
        );
    }

}