<?php

namespace App\Domain\Item;

class File
{
    /** @var string */
    protected $src;

    /** @var string */
    protected $originalName;

    /** @var string */
    protected $mimeType;

    /** @var int */
    protected $size;

    /** @var array */
    protected $dimensions;

    public function getSrc(): ?string
    {
        return $this->src;
    }

    public function setSrc(?string $src): void
    {
        $this->src = $src;
    }

    public function getOriginalName(): ?string
    {
        return $this->originalName;
    }

    public function setOriginalName(?string $originalName): void
    {
        $this->originalName = $originalName;
    }

    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    public function setMimeType(?string $mimeType): void
    {
        $this->mimeType = $mimeType;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function setSize(?int $size): void
    {
        $this->size = $size;
    }

    public function getDimensions(): ?array
    {
        return $this->dimensions;
    }

    public function setDimensions(?array $dimensions): void
    {
        $this->dimensions = $dimensions;
    }
}