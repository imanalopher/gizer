<?php

namespace App\Domain\Item;

class Meta
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var Item
     */
    protected $item;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $wallet;

    /**
     * @var string
     */
    protected $use;

    /**
     * @var string
     */
    protected $category;

    /**
     * @var string
     */
    protected $rarity;

    /**
     * @var string
     */
    protected $origin;

    /**
     * @var \DateTime
     */
    protected $released;

    /**
     * @var \DateTime
     */
    protected $unlocked;

    /**
     * @var string
     */
    protected $collection;

    /**
     * @var Series
     */
    protected $series;

    public function __construct()
    {
        $this->released = new \DateTime();
    }

    /**
     * @return string
     */
    public function getName():? string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Item
     */
    public function getItem():? Item
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem(Item $item): void
    {
        $this->item = $item;
    }

    /**
     * @return string
     */
    public function getDescription():? string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getWallet():? string
    {
        return $this->wallet;
    }

    /**
     * @param string $wallet
     */
    public function setWallet(string $wallet): void
    {
        $this->wallet = $wallet;
    }

    /**
     * @return string
     */
    public function getUse():? string
    {
        return $this->use;
    }

    /**
     * @param string $use
     */
    public function setUse(string $use): void
    {
        $this->use = $use;
    }

    /**
     * @return string
     */
    public function getCategory():? string
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category): void
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getRarity():? string
    {
        return $this->rarity;
    }

    /**
     * @param string $rarity
     */
    public function setRarity(string $rarity): void
    {
        $this->rarity = $rarity;
    }

    /**
     * @return string
     */
    public function getOrigin():? string
    {
        return $this->origin;
    }

    /**
     * @param string $origin
     */
    public function setOrigin(string $origin): void
    {
        $this->origin = $origin;
    }

    /**
     * @return \DateTime
     */
    public function getReleased():? \DateTime
    {
        return $this->released;
    }

    /**
     * @param \DateTime $released
     */
    public function setReleased(?\DateTime $released): void
    {
        $this->released = $released;
    }

    /**
     * @return \DateTime
     */
    public function getUnlocked():? \DateTime
    {
        return $this->unlocked;
    }

    /**
     * @param \DateTime $unlocked
     */
    public function setUnlocked(?\DateTime $unlocked): void
    {
        $this->unlocked = $unlocked;
    }

    /**
     * @return string
     */
    public function getCollection():? string
    {
        return $this->collection;
    }

    /**
     * @param string $collection
     */
    public function setCollection(string $collection): void
    {
        $this->collection = $collection;
    }

    /**
     * @return Series
     */
    public function getSeries():? Series
    {
        return $this->series;
    }

    /**
     * @param Series $series
     */
    public function setSeries(Series $series): void
    {
        $this->series = $series;
    }


}