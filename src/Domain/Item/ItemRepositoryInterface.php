<?php

namespace App\Domain\Item;

interface ItemRepositoryInterface
{
    public function save(Item $item);
    public function find($id, $lockMode = \Doctrine\ODM\MongoDB\LockMode::NONE, $lockVersion = NULL);
    public function findAll();
    public function remove(Item $item);
    public function findAllByCriteria(Model\Criteria $criteria);
}