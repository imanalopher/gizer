<?php

namespace App\Domain\Item;

class Model
{
    /**
     * @var string
     */
    protected $src;

    /**
     * @var Resources
     */
    protected $resources;

    /**
     * @return string
     */
    public function getSrc():? string
    {
        return $this->src;
    }

    /**
     * @param string $src
     */
    public function setSrc(string $src): void
    {
        $this->src = $src;
    }

    /**
     * @return Resources
     */
    public function getResources()
    {
        return $this->resources;
    }

    /**
     * @param Resources $resources
     */
    public function setResources(Resources $resources): void
    {
        $this->resources = $resources;
    }
}