<?php

namespace App\Domain\Item;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

class Current
{
    const STATUS_LOCKED = 'locked';
    const STATUS_UNLOCKED = 'unlocked';

    /**
     * @var string
     */
    protected $status;

    /**
     * @var int
     */
    protected $likes;

    /**
     * @var Item[]
     */
    protected $similar;

    /**
     * @var Item
     */
    protected $item;

    /**
     * Current constructor.
     */
    public function __construct()
    {
        $this->similar = new ArrayCollection();
        $this->status = self::STATUS_LOCKED;
        $this->likes = 0;
    }

    /**
     * @return string
     */
    public function getStatus():? string
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getLikes():? int
    {
        return $this->likes;
    }

    /**
     * @return Collection
     */
    public function getSimilar():? Collection
    {
        return $this->similar;
    }

    /**
     * @param Item $item
     */
    public function addSimilar($item): void
    {
        if (!$this->similar->contains($item)) {
            $this->similar->add($item);
        }
    }

    /**
     * @param Item $item
     */
    public function removeSimilar($item): void
    {
        if ($this->similar->contains($item)) {
            $this->similar->removeElement($item);
        }
    }

    /**
     * @param int $likes
     */
    public function setLikes(int $likes): void
    {
        $this->likes = $likes;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return Item
     */
    public function getItem():? Item
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem(Item $item): void
    {
        $this->item = $item;
    }

    /**
     * @param Collection|Item[] $similar
     */
    public function setSimilar(Collection $similar): void
    {
        $this->similar = $similar;
    }


}