<?php

namespace App\Domain\Item;

use App\Domain\Resource\ResourcesType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('resources', ResourcesType::class)
            ->add('current', CurrentType::class)
            ->add('meta', MetaType::class)
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => \App\Document\Item::class
        ]);
    }
}