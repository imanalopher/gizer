<?php

namespace App\Controller;

class SecurityController
{
    /**
     * @throws \Exception
     */
    public function login()
    {
        throw new \Exception('Action should not be invoked');
    }

}