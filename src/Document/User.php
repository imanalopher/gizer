<?php

declare(strict_types=1);

namespace App\Document;

class User extends \App\Domain\User\User
{
    /**
     * @var string
     */
    private $id;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

}