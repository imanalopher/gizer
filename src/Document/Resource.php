<?php

declare(strict_types=1);

namespace App\Document;

class Resource extends \App\Domain\Resource\Resource
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}