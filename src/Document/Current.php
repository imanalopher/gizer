<?php

namespace App\Document;

class Current extends \App\Domain\Item\Current
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}