<?php

declare(strict_types=1);

namespace App\Document;

class Item extends \App\Domain\Item\Item
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}
