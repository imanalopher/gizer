<?php

namespace App\Document;

class Chest extends \App\Domain\Chest\Chest
{
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

}