<?php

declare(strict_types=1);

namespace App\Document;

class Transaction extends \App\Domain\User\Transaction\Transaction
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}