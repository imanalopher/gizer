<?php

namespace App\Document;

use FOS\OAuthServerBundle\Document\Client as BaseClient;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Document\AuthCode;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class Client extends BaseClient
{
    protected $id;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}