<?php

declare(strict_types=1);

namespace App\Document;

class Model extends \App\Domain\Item\Model
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}
