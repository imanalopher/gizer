<?php

namespace App\Repository;

use App\Domain\Chest\ChestRepositoryInterface;
use App\Domain\Chest\Chest;
use App\Domain\Chest\Model\Criteria;
use Doctrine\ODM\MongoDB\DocumentRepository;

class ChestRepository extends DocumentRepository implements ChestRepositoryInterface
{
    public function save(Chest $chest)
    {
        $this->dm->persist($chest);
        $this->dm->flush();
    }

    public function remove(Chest $chest)
    {
        $this->dm->remove($chest);
        $this->dm->flush($chest);
    }

    public function getRandom()
    {
        $qb = $this->createQueryBuilder();
        $count = $qb->getQuery()->count();

        if ($count) {
            $skip = random_int(0, $count-1);
        } else {
            $skip = 0;
        }

        $qb->skip($skip);

        $qb->field('status')->equals(Chest::STATUS_LOCKED);

        return $qb->getQuery()->getSingleResult();
    }

    public function findAllByCriteria(Criteria $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->eagerCursor(true);

        $limit = $criteria->getLimit();
        $page = $criteria->getPage();

        if (is_numeric($limit)) {

            $qb->limit($limit);

            if (is_numeric($page)) {
                $qb->skip($limit * ($page-1));
            }
        }

        if ($list = $criteria->getList()) {
            $qb->field('_id')->in($list);
        }

        if ($q = $criteria->getQ()) {
            $qb->field('name')->equals(new \MongoRegex(sprintf('/.*%s.*/i', $q)));
        }

        return $qb->getQuery()->execute();
    }
}