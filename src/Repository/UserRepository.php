<?php

namespace App\Repository;

use App\Domain\User\Model\Criteria;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ODM\MongoDB\DocumentRepository;

class UserRepository extends DocumentRepository implements UserLoaderInterface, UserRepositoryInterface
{
    /**
     * @param string $username
     * @return mixed|null|\Symfony\Component\Security\Core\User\UserInterface
     */
    public function loadUserByUsername($username)
    {
        $qb = $this->createQueryBuilder();

        $qb->addOr($qb->expr()->field('username')->equals($username), $qb->expr()->field('email')->equals($username));

        return $qb->getQuery()->getSingleResult();
    }

    public function save(User $user)
    {
        $this->dm->persist($user);
        $this->dm->flush();
    }

    public function remove(User $user)
    {
        $this->dm->remove($user);
        $this->dm->flush($user);
    }

    public function findAllByCriteria(Criteria $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->eagerCursor(true);

        $limit = $criteria->getLimit();
        $page = $criteria->getPage();

        if ($wallet = $criteria->getWallet()) {
            $qb->field('gzr.id')->equals($wallet);
        }

        if (is_numeric($limit)) {

            $qb->limit($limit);

            if (is_numeric($page)) {
                $qb->skip($limit * ($page-1));
            }
        }

        if ($list = $criteria->getList()) {
            $qb->field('_id')->in($list);
        }

        if ($q = $criteria->getQ()) {
            $qb->field('nick')->equals(new \MongoRegex(sprintf('/.*%s.*/i', $q)));
        }

        return $qb->getQuery()->execute();
    }
}