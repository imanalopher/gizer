<?php

namespace App\Repository;

use App\Domain\Resource\Model\Criteria;
use App\Domain\Resource\Resource;
use App\Domain\Resource\ResourceRepositoryInterface;
use Doctrine\ODM\MongoDB\DocumentRepository;

class ResourceRepository extends DocumentRepository implements ResourceRepositoryInterface
{
    public function save(Resource $resource)
    {
        $this->dm->persist($resource);
        $this->dm->flush();
    }

    public function remove(Resource $resource)
    {
        $this->dm->remove($resource);
        $this->dm->flush();
    }

    public function findAllByCriteria(Criteria $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->eagerCursor(true);

        $limit = $criteria->getLimit();
        $page = $criteria->getPage();

        if (is_numeric($limit)) {

            $qb->limit($limit);

            if (is_numeric($page)) {
                $qb->skip($limit * ($page-1));
            }
        }

        if ($list = $criteria->getList()) {
            $qb->field('_id')->in($list);
        }

        if ($q = $criteria->getQ()) {
            $qb->field('file.src')->equals(new \MongoRegex(sprintf('/.*%s.*/i', $q)));
        }

        return $qb->getQuery()->execute();
    }
}