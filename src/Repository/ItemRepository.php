<?php

namespace App\Repository;

use App\Domain\Item\Model\Criteria;
use App\Domain\Item\Item;
use App\Domain\Item\ItemRepositoryInterface;
use Doctrine\ODM\MongoDB\DocumentRepository;

class ItemRepository extends DocumentRepository implements ItemRepositoryInterface
{
    public function save(Item $item)
    {
        $this->dm->persist($item);
        $this->dm->flush();
    }

    public function remove(Item $item)
    {
        $this->dm->remove($item);
        $this->dm->flush($item);
    }

    public function findAllByCriteria(Criteria $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->eagerCursor(true);

        $limit = $criteria->getLimit();
        $page = $criteria->getPage();

        if (is_numeric($limit)) {

            $qb->limit($limit);

            if (is_numeric($page)) {
                $qb->skip($limit * ($page-1));
            }
        }

        if ($list = $criteria->getList()) {
            $qb->field('_id')->in($list);
        }

        if ($q = $criteria->getQ()) {
            $qb->field('meta.name')->equals(new \MongoRegex(sprintf('/.*%s.*/i', $q)));
        }

        return $qb->getQuery()->execute();
    }
}