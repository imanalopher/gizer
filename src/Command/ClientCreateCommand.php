<?php
namespace App\Command;

use FOS\OAuthServerBundle\Document\ClientManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ClientCreateCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'acme:oauth:client:create';
    private $client_manager;

    public function __construct(ClientManager $client_manager)
    {
        parent::__construct();
        $this->client_manager = $client_manager;
    }

    protected function configure ()
    {
        $this
            ->setName('acme:oauth:client:create')
            ->setDescription('Creates a new client')
            ->addOption('redirect-uri', null, InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'Sets the redirect uri. Use multiple times to set multiple uris.', null)
            ->addOption('grant-type', null, InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'Set allowed grant type. Use multiple times to set multiple grant types', null)
        ;
    }
    protected function execute (InputInterface $input, OutputInterface $output)
    {
        $clientManager = $this->client_manager;
        $client = $clientManager->createClient();
        $client->setRedirectUris($input->getOption('redirect-uri'));
        $client->setAllowedGrantTypes($input->getOption('grant-type'));
        $clientManager->updateClient($client);

        $output->writeln('');
        $output->writeln('');
        $output->writeln('');
        $output->writeln('');

        $output->writeln(sprintf('client_id = %s', $client->getPublicId()));
        $output->writeln(sprintf('client_secret = %s', $client->getSecret()));
    }
}
