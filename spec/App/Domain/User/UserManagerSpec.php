<?php

namespace spec\App\Domain\User;

use App\Document\User;
use App\Domain\User\UserFactory;
use App\Domain\User\UserManager;
use App\Repository\UserRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @mixin UserManager
 */
class UserManagerSpec extends ObjectBehavior
{
    function let(
        UserRepository $userRepository,
        UserPasswordEncoderInterface $passwordEncoder,
        UserFactory $factory
    )   : void {
        $this->beConstructedWith($userRepository, $passwordEncoder, $factory);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(UserManager::class);
    }

    function it_should_create_user_and_encode_password(
        User $user,
        UserPasswordEncoderInterface $passwordEncoder,
        UserRepository $userRepository,
        UserFactory $factory
    ) {
        $password = 'password';
        $username = 'username';
        $email = 'email';
        $encodedPassword = 'encodedPassword';

        $factory->create()->willReturn($user);

        $user->setUsername($username)->shouldBeCalled();
        $user->setEmail($email)->shouldBeCalled();

        $passwordEncoder->encodePassword($user, $password)->willReturn($encodedPassword);
        $user->setPassword($encodedPassword)->shouldBeCalled();
        $user->getGzr()->shouldBeCalled();

        $userRepository->save($user)->shouldBeCalled();

        $this->create($username, $email, $password);
    }
}
