<?php

namespace App\Tests\Behat\Service\Mock\App\Domain\SendGrid;

use App\Domain\SendGrid\RequestSenderInterface;
use Monolog\Logger;
use Psr\Http\Message\RequestInterface;

class RequestSender implements RequestSenderInterface
{
    /**
     * @var Logger
     */
    protected $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function send(RequestInterface $request)
    {
        $this->logger->debug(json_encode([
            'method' => $request->getMethod(),
            'uri' => (string) $request->getUri(),
            'headers' => $request->getHeaders(),
            'body' => json_decode($request->getBody()->getContents())
        ]));

        if (strpos((string) $request->getUri(), 'contactdb/recipients')) {

            return (object) [
                'persisted_recipients' => [
                    123456
                ]
            ];

        }
    }
}