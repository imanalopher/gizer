<?php

namespace App\Tests\Behat\Context\Item;

use App\Document\Item;
use App\Domain\Item\ItemManager;
use App\Tests\Behat\Service\SharedStorage;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behatch\HttpCall\Request;

class ItemContext implements Context, SnippetAcceptingContext
{
    /**
     * @var ItemManager
     */
    private $itemManager;

    /**
     * @var SharedStorage
     */
    private $sharedStorage;

    public function __construct(
        ItemManager $itemManager,
        SharedStorage $sharedStorage
    ) {
        $this->itemManager = $itemManager;
        $this->sharedStorage = $sharedStorage;
    }

    /**
     * @Given there are Item as :key
     */
    public function thereAreItemAs($key)
    {
        $item = $this->itemManager->create();

        $this->sharedStorage->set('item', $item);

        $this->sharedStorage->set($key, $item);
    }

    /**
     * @Given the Item has model as :key
     */
    public function theItemHasModelAs($src)
    {
        /** @var Item $item */
        if ($item = $this->sharedStorage->get('item')) {
            $item->getResources()->getModel()->setSrc($src);
            $this->itemManager->save($item);
        }
    }

    /**
     * @Given the Item current status is :status
     */
    public function theItemCurrentStatusIs($status)
    {
        /** @var Item $item */
        if ($item = $this->sharedStorage->get('item')) {
            $item->getCurrent()->setStatus($status);
            $this->itemManager->save($item);
        }
    }

    /**
     * @Given the Item current likes count is :likes
     */
    public function theItemCurrentLikesCountIs($likes)
    {
        /** @var Item $item */
        if ($item = $this->sharedStorage->get('item')) {
            $item->getCurrent()->setLikes($likes);
            $this->itemManager->save($item);
        }
    }

    /**
     * @Given the Item is :no th of :of in own series
     */
    public function theItemIsOfInOwnSeries($no, $of)
    {
        /** @var Item $item */
        if ($item = $this->sharedStorage->get('item')) {
            $item->getMeta()->getSeries()->setItem($no);
            $item->getMeta()->getSeries()->setOf($of);
            $this->itemManager->save($item);
        }
    }

    /**
     * @Given there are similar as following:
     * @Given the Item current similar is as following:
     */
    public function theItemCurrentSimilarIsAsFollowing(TableNode $similars)
    {
        foreach ($similars->getColumnsHash() as $key => $row) {
            $similar = $this->itemManager->create();

            $this->sharedStorage->set($row['id'], $similar);

            if ($this->sharedStorage->has('item')) {
                /** @var Item $item */
                $item = $this->sharedStorage->get('item');

                $item->getCurrent()->addSimilar($similar);
                $this->itemManager->save($item);
            }
        }
    }

    /**
     * @Given the Item has meta as following:
     */
    public function theItemHasMetaAsFollowing(TableNode $table)
    {
        /** @var Item $item */
        $item = $this->sharedStorage->get('item');

        foreach ($table->getRowsHash() as $key => $row) {

            $method = sprintf('set%s', ucfirst($key));

            if (in_array($key, ['unlocked', 'released'])) {
                $row = new \DateTime($row);
            }

            $item->getMeta()->$method($row);

        }
        $this->itemManager->save($item);
    }
}