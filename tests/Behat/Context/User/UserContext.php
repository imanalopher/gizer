<?php

namespace App\Tests\Behat\Context\User;

use App\Document\User;
use App\Domain\User\Address\AddressManager;
use App\Domain\User\Avatar\AvatarManager;
use App\Domain\User\Gzr\GzrManager;
use App\Domain\User\Social\SocialManager;
use App\Domain\User\Transaction\TransactionManager;
use App\Domain\User\UserManager;
use App\Tests\Behat\Service\SharedStorage;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\TableNode;

class UserContext implements Context, SnippetAcceptingContext
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var SharedStorage
     */
    private $sharedStorage;

    /**
     * @var AddressManager
     */
    private $addressManager;

    /**
     * @var AvatarManager
     */
    private $avatarManager;

    /**
     * @var GzrManager
     */
    private $gzrManager;

    /**
     * @var SocialManager
     */
    private $socialManager;

    /**
     * @var TransactionManager
     */
    private $transactionManager;

    public function __construct(
        AddressManager $addressManager,
        AvatarManager $avatarManager,
        GzrManager $gzrManager,
        SocialManager $socialManager,
        UserManager $userManager,
        SharedStorage $sharedStorage,
        TransactionManager $transactionManager
    ) {
        $this->userManager = $userManager;
        $this->sharedStorage = $sharedStorage;
        $this->addressManager = $addressManager;
        $this->avatarManager = $avatarManager;
        $this->gzrManager = $gzrManager;
        $this->socialManager = $socialManager;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @Given there are User as :key
     * @Given there are Users as :key with the following details:

     */
    public function thereAreUserAs($key, TableNode $users)
    {
        $user = $this->thereAreUsersWithTheFollowingDetails($users);

        $this->sharedStorage->set($key, $user);
        $this->sharedStorage->set('user', $user);
    }
    
    /**
     * @Given there are Users with the following details:
     */
    public function thereAreUsersWithTheFollowingDetails(TableNode $users)
    {
        foreach ($users->getColumnsHash() as $key => $user) {

            $user = $this->userManager->create(
                $user['username'],
                $user['email'],
                $user['password']
            );
        }

        return $user;
    }

    /**
     * @Given the User has Address as following:
     */
    public function theUserHasAddressAsFollowing(TableNode $table)
    {
        foreach ($table->getColumnsHash() as $key => $node) {

            $address = $this->addressManager->create(
                $node['street'],
                $node['city'],
                $node['country']
            );

            if ($this->sharedStorage->has('user')) {
                /** @var User $user */
                $user = $this->sharedStorage->get('user');
                $user->setAddress($address);
                $this->userManager->save($user);
            }
        }
    }

    /**
     * @Given the User has interests as following:
     */
    public function theUserHasInterestsAsFollowing(TableNode $table)
    {
        $interests = [];

        foreach ($table->getColumnsHash() as $key => $node) {

            $interests[] = $node['name'];

        }

        if ($this->sharedStorage->has('user')) {
            /** @var User $user */
            $user = $this->sharedStorage->get('user');
            $user->setInterests($interests);
            $this->userManager->save($user);
        }
    }

    /**
     * @Given the User has Gzr as following:
     */
    public function theUserHasGzrAsFollowing(TableNode $table)
    {
        foreach ($table->getColumnsHash() as $key => $node) {

            $gzr = $this->gzrManager->create(
                $node['type'],
                $node['id'],
                $node['amount']
            );

            $this->sharedStorage->set($node['id'], $gzr);

            if ($this->sharedStorage->has('user')) {
                /** @var User $user */
                $user = $this->sharedStorage->get('user');
                $user->setGzr($gzr);
                $this->userManager->save($user);
            }

        }
    }

    /**
     * @Given the User has Avatar as following:
     */
    public function theUserHasAvatarAsFollowing(TableNode $table)
    {
        foreach ($table->getColumnsHash() as $key => $node) {

            $avatar = $this->avatarManager->create(
                $node['type'],
                $node['uri']
            );

            if ($this->sharedStorage->has('user')) {
                /** @var User $user */
                $user = $this->sharedStorage->get('user');
                $user->setAvatar($avatar);
                $this->userManager->save($user);
            }

        }
    }

    /**
     * @Given the User has Social as following:
     */
    public function theUserHasSocialAsFollowing(TableNode $table)
    {
        foreach ($table->getColumnsHash() as $key => $node) {

            $social = $this->socialManager->create(
                $node['name'],
                $node['url']
            );

            if ($this->sharedStorage->has('user')) {
                /** @var User $user */
                $user = $this->sharedStorage->get('user');
                $user->addSocial($social);
                $this->userManager->save($user);
            }

        }
    }

    /**
     * @Given the User has Transactions as following:
     */
    public function theUserHasTransactionsAsFollowing(TableNode $table)
    {
        foreach ($table->getColumnsHash() as $key => $node) {

            $transaction = $this->transactionManager->create(
                $node['txId'],
                $node['eth'],
                $node['gzr'],
                new \DateTime($node['confirmedAt'])
            );

            if ($this->sharedStorage->has('user')) {
                /** @var User $user */
                $user = $this->sharedStorage->get('user');
                $user->addTransaction($transaction);

                $this->userManager->save($user);
            }

        }
    }

    /**
     * @Given the User knows:
     */
    public function theUserKnows(TableNode $table)
    {
        $knows = [];

        foreach ($table->getColumnsHash() as $key => $node) {

            $knows[] = $node['id'];

        }

        if ($this->sharedStorage->has('user')) {
            /** @var User $user */
            $user = $this->sharedStorage->get('user');
            $user->setKnows($knows);
            $this->userManager->save($user);
        }
    }

    /**
     * @Given the User owns:
     */
    public function theUserOwns(TableNode $table)
    {
        $knows = [];

        foreach ($table->getColumnsHash() as $key => $node) {

            $knows[] = $node['id'];

        }

        if ($this->sharedStorage->has('user')) {
            /** @var User $user */
            $user = $this->sharedStorage->get('user');
            $user->setOwns($knows);
            $this->userManager->save($user);
        }
    }

    /**
     * @Given the User details are as following:
     */
    public function theUserHasMetaAsFollowing(TableNode $table)
    {
        /** @var User $user */
        $user = $this->sharedStorage->get('user');

        foreach ($table->getRowsHash() as $key => $row) {

            $method = sprintf('set%s', ucfirst($key));

            if (in_array($key, ['active', 'reputation', 'boosts'])) {
                $row = (int) $row;
            }

            $user->$method($row);

        }
        $this->userManager->save($user);
    }
}