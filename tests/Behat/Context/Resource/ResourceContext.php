<?php

namespace App\Tests\Behat\Context\Resource;

use App\Document\Item;
use App\Domain\Item\ItemManager;
use App\Domain\Resource\ResourceManager;
use App\Tests\Behat\Service\SharedStorage;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\TableNode;

class ResourceContext implements Context, SnippetAcceptingContext
{
    /**
     * @var ResourceManager
     */
    private $resourceManager;

    /**
     * @var SharedStorage
     */
    private $sharedStorage;

    /**
     * @var ItemManager
     */
    private $itemManager;

    public function __construct(
        ResourceManager $resourceManager,
        SharedStorage $sharedStorage,
        ItemManager $itemManager
    ) {
        $this->resourceManager = $resourceManager;
        $this->sharedStorage = $sharedStorage;
        $this->itemManager = $itemManager;
    }

    /**
     * @Given there are Icons with the following details:
     * @Given the Item has Icons with the following details:
     */
    public function thereAreIconsWithTheFollowingDetails(TableNode $users)
    {
        foreach ($users->getColumnsHash() as $key => $item) {

            $icon = $this->resourceManager->create(
                $item['source']
            );

            $this->resourceManager->save($icon);

            if ($this->sharedStorage->has('item')) {
                /** @var Item $item */
                $item = $this->sharedStorage->get('item');
                $item->getResources()->addIcon($icon);

                $this->itemManager->save($item);
            }
        }
    }

    /**
     * @Given there are Images with the following details:
     * @Given the Item has Images with the following details:
     */
    public function thereAreImagesWithTheFollowingDetails(TableNode $users)
    {
        foreach ($users->getColumnsHash() as $key => $item) {

            $image = $this->resourceManager->create(
                $item['source']
            );

            $this->resourceManager->save($image);

            if ($this->sharedStorage->has('item')) {
                /** @var Item $item */
                $item = $this->sharedStorage->get('item');
                $item->getResources()->addImage($image);

                $this->itemManager->save($item);
            }
        }
    }

    /**
     * @Given there are Videos with the following details:
     * @Given the Item has Videos with the following details:
     */
    public function thereAreVideosWithTheFollowingDetails(TableNode $users)
    {
        $videos = [];

        foreach ($users->getColumnsHash() as $key => $item) {

            $videos[] = $item['source'];

        }

        if ($this->sharedStorage->has('item')) {
            /** @var Item $item */
            $item = $this->sharedStorage->get('item');
            $item->getResources()->setVideos($videos);

            $this->itemManager->save($item);
        }
    }
}