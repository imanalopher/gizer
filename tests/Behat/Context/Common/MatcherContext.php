<?php

namespace App\Tests\Behat\Context\Common;

use App\Tests\Behat\Service\SharedStorage;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behatch\HttpCall\HttpCallResultPool;
use Coduo\PHPMatcher\Factory\SimpleFactory;

class MatcherContext implements Context
{
    /**
     * @var HttpCallResultPool
     */
    private $httpCallResultPool;

    /**
     * @var SharedStorage
     */
    private $sharedStorage;

    public function __construct(
        SharedStorage $sharedStorage,
        HttpCallResultPool $httpCallResultPool
    ) {
        $this->httpCallResultPool = $httpCallResultPool;
        $this->sharedStorage = $sharedStorage;
    }

    /**
     * @Then the JSON should match pattern:
     *
     * @param PyStringNode $string
     * @throws \Exception
     */
    public function theJsonShouldMatchPattern(PyStringNode $string)
    {
        $expected = $string->getRaw();
        $current = $this->httpCallResultPool->getResult()->getValue();

        foreach (array_keys($this->sharedStorage->getClipboard()) as $key) {
            if (in_array($key, ['chest', 'item', 'user'])) {
                continue;
            }

            $current = str_replace(
                $this->sharedStorage->get($key)->getId(),
                $key,
                $current
            );

        }

        $matcher = (new SimpleFactory())->createMatcher();

        if (!$matcher->match($current, $expected)) {
            throw new \Exception($matcher->getError());
        }
    }
}