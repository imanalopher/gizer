<?php

namespace App\Tests\Behat\Context\Common;

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Defines application features from the specific context.
 */
class DoctrineContext implements Context, SnippetAcceptingContext
{
    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $manager;

    /**
     * @var \Doctrine\ODM\MongoDB\SchemaManager
     */
    private $schemaTool;

    /**
     * @var array
     */
    private $classes;

    /**
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
        $this->manager = $doctrine->getManager();
        $this->schemaTool = $this->manager->getSchemaManager();
        $this->classes = $this->manager->getMetadataFactory()->getAllMetadata();
    }

    /**
     * @BeforeScenario @createSchema
     */
    public function createDatabase()
    {
        $this->schemaTool->createDatabases();
        $this->schemaTool->createCollections();
        $this->schemaTool->ensureIndexes();
    }

    /**
     * @AfterScenario @dropSchema
     */
    public function dropDatabase()
    {
        $this->schemaTool->deleteIndexes();
        $this->schemaTool->dropCollections();
        $this->schemaTool->dropDatabases();
    }
}
