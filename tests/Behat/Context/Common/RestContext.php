<?php

namespace App\Tests\Behat\Context\Common;

use App\Tests\Behat\Service\SharedStorage;
use Behat\Gherkin\Node\PyStringNode;
use Behatch\HttpCall\HttpCallResultPool;
use Coduo\PHPMatcher\Factory\SimpleFactory;
use Behatch\HttpCall\Request;

class RestContext extends \Behatch\Context\RestContext
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var SharedStorage
     */
    private $sharedStorage;

    public function __construct(
        SharedStorage $sharedStorage,
        Request $request
    ) {
        $this->request = $request;
        $this->sharedStorage = $sharedStorage;

        parent::__construct($request);
    }

    public function iSendARequestTo($method, $url, PyStringNode $body = null, $files = [])
    {
        $body = $body ? $body->getRaw() : null;

        foreach (array_keys($this->sharedStorage->getClipboard()) as $key) {

            if (in_array($key, ['chest', 'item', 'user'])) {
                continue;
            }

            $url = str_replace(
                $key,
                $this->sharedStorage->get($key)->getId(),
                $url
            );

            if ($body) {
                $body = str_replace(
                    $key,
                    $this->sharedStorage->get($key)->getId(),
                    $body
                );
            }
        }

        return $this->request->send(
            $method,
            $this->locatePath($url),
            [],
            $files,
            $body
        );
    }

}