<?php

namespace App\Tests\Behat\Context\Chest;

use App\Document\Chest;
use App\Domain\Chest\ChestManager;
use App\Domain\Item\ItemManager;
use App\Domain\User\UserManager;
use App\Tests\Behat\Service\SharedStorage;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behatch\HttpCall\Request;

class ChestContext implements Context, SnippetAcceptingContext
{
    /**
     * @var ChestManager
     */
    private $chestManager;

    /**
     * @var SharedStorage
     */
    private $sharedStorage;
    
    /**
     * @var ItemManager
     */
    private $itemManager;

    /**
     * @var UserManager
     */
    private $userManager;

    public function __construct(
        ChestManager $chestManager,
        SharedStorage $sharedStorage,
        ItemManager $itemManager,
        UserManager $userManager
    ) {
        $this->chestManager = $chestManager;
        $this->sharedStorage = $sharedStorage;
        $this->itemManager = $itemManager;
        $this->userManager = $userManager;
    }

    /**
     * @Given there are Chest as :key
     */
    public function thereAreChestAs($key)
    {
        $chest = $this->chestManager->create();

        $this->sharedStorage->set('chest', $chest);
        $this->sharedStorage->set($key, $chest);
    }

    /**
     * @Given the Chest details are as following:
     */
    public function theChestHasMetaAsFollowing(TableNode $table)
    {
        /** @var Chest $chest */
        $chest = $this->sharedStorage->get('chest');

        foreach ($table->getRowsHash() as $key => $row) {

            $method = sprintf('set%s', ucfirst($key));

            if (in_array($key, ['price'])) {
                $row = (int) $row;
            }

            $chest->$method($row);

        }
        $this->chestManager->save($chest);
    }

    /**
     * @Given the Chest Items as following:
     */
    public function theChestItemsAsFollowing(TableNode $table)
    {
        foreach ($table->getColumnsHash() as $key => $row) {

            $item = $this->itemManager->create();
            $this->sharedStorage->set($row['id'], $item);

            if ($this->sharedStorage->has('chest')) {

                /** @var Chest $chest */
                $chest = $this->sharedStorage->get('chest');
                $chest->addItem($item);

                $this->chestManager->save($chest);
            }
        }

    }

    /**
     * @Given the Chest owner is :key
     */
    public function theChestOwnerIs($key)
    {
        $user = $this->userManager->create(sprintf('chest %s owner', $key), sprintf('chest%sOwner@example.com', $key), 'password');

        if ($this->sharedStorage->has('chest')) {

            /** @var Chest $chest */
            $chest = $this->sharedStorage->get('chest');
            $chest->setUser($user);

            $this->chestManager->save($chest);
        }

    }
}