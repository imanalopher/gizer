Feature: Handle item via the RESTful API

  In order to sell items
  As a software owner
  I need to able to let admins discovery resources

  Background:
    Given I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"

  @createSchema @dropSchema
  Scenario: get resources
    Given there are Icons with the following details:
      | source                                  |
      | http://s3.amazonaws.com/gizer/icon1.jpg |
      | http://s3.amazonaws.com/gizer/icon2.jpg |
      | http://s3.amazonaws.com/gizer/icon3.jpg |
      | http://s3.amazonaws.com/gizer/icon4.jpg |
      | http://s3.amazonaws.com/gizer/icon5.jpg |

    And there are Images with the following details:
      | source                                   |
      | http://s3.amazonaws.com/gizer/image1.jpg |
      | http://s3.amazonaws.com/gizer/image2.jpg |
      | http://s3.amazonaws.com/gizer/image3.jpg |
      | http://s3.amazonaws.com/gizer/image4.jpg |
      | http://s3.amazonaws.com/gizer/image5.jpg |

    When I send a "GET" request to "/resource"
    Then the JSON should match pattern:
    """
    [
      "http://s3.amazonaws.com/gizer/icon1.jpg",
      "http://s3.amazonaws.com/gizer/icon2.jpg",
      "http://s3.amazonaws.com/gizer/icon3.jpg",
      "http://s3.amazonaws.com/gizer/icon4.jpg",
      "http://s3.amazonaws.com/gizer/icon5.jpg",
      "http://s3.amazonaws.com/gizer/image1.jpg",
      "http://s3.amazonaws.com/gizer/image2.jpg",
      "http://s3.amazonaws.com/gizer/image3.jpg",
      "http://s3.amazonaws.com/gizer/image4.jpg",
      "http://s3.amazonaws.com/gizer/image5.jpg"
    ]
    """

  @createSchema @dropSchema
  Scenario: get resources - filter by query
    Given there are Icons with the following details:
      | source                                  |
      | http://s3.amazonaws.com/gizer/icon1.jpg |
      | http://s3.amazonaws.com/gizer/icon2.jpg |
      | http://s3.amazonaws.com/gizer/icon3.jpg |
      | http://s3.amazonaws.com/gizer/icon4.jpg |
      | http://s3.amazonaws.com/gizer/icon5.jpg |

    And there are Images with the following details:
      | source                                   |
      | http://s3.amazonaws.com/gizer/image1.jpg |
      | http://s3.amazonaws.com/gizer/image2.jpg |
      | http://s3.amazonaws.com/gizer/image3.jpg |
      | http://s3.amazonaws.com/gizer/image4.jpg |
      | http://s3.amazonaws.com/gizer/image5.jpg |

    When I send a "GET" request to "/resource?q=image"
    Then the JSON should match pattern:
    """
    [
      "http://s3.amazonaws.com/gizer/image1.jpg",
      "http://s3.amazonaws.com/gizer/image2.jpg",
      "http://s3.amazonaws.com/gizer/image3.jpg",
      "http://s3.amazonaws.com/gizer/image4.jpg",
      "http://s3.amazonaws.com/gizer/image5.jpg"
    ]
    """