Feature: Handle item via the RESTful API

  In order to sell items
  As a software owner
  I need to able to let users discovery items

  Background:
    Given I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"

  @createSchema @dropSchema
  Scenario: create item
      Given there are Icons with the following details:
        | source                                  |
        | http://s3.amazonaws.com/gizer/icon1.jpg |
        | http://s3.amazonaws.com/gizer/icon2.jpg |
        | http://s3.amazonaws.com/gizer/icon3.jpg |
        | http://s3.amazonaws.com/gizer/icon4.jpg |
        | http://s3.amazonaws.com/gizer/icon5.jpg |

      And there are Images with the following details:
        | source                                   |
        | http://s3.amazonaws.com/gizer/image1.jpg |
        | http://s3.amazonaws.com/gizer/image2.jpg |
        | http://s3.amazonaws.com/gizer/image3.jpg |
        | http://s3.amazonaws.com/gizer/image4.jpg |
        | http://s3.amazonaws.com/gizer/image5.jpg |

      And there are Videos with the following details:
        | source                            |
        | https://www.youtube.com/q=sdUdw83 |

      When I send a "PUT" request to "/item" with body:
      """
      {
          "resources": {
              "icons": [
                  "http://s3.amazonaws.com/gizer/icon1.jpg",
                  "http://s3.amazonaws.com/gizer/icon2.jpg",
                  "http://s3.amazonaws.com/gizer/icon3.jpg",
                  "http://s3.amazonaws.com/gizer/icon4.jpg",
                  "http://s3.amazonaws.com/gizer/icon5.jpg"
              ],
              "images": [
                  "http://s3.amazonaws.com/gizer/image1.jpg",
                  "http://s3.amazonaws.com/gizer/image2.jpg",
                  "http://s3.amazonaws.com/gizer/image3.jpg",
                  "http://s3.amazonaws.com/gizer/image4.jpg",
                  "http://s3.amazonaws.com/gizer/image5.jpg"
              ],
              "videos": [
                  "https://www.youtube.com/q=sdUdw83"
              ],
              "model": {
                  "src": "http://s3.amazonaws.com/gizer/models/bow.fbx"
              }
          },
          "current": {
              "similar": [
                  "4ddb3646-ebda-11e7-8c3f-9a214cf093ae",
                  "4ddb3646-ebda-11e7-8c3f-9a214cf093ae",
                  "4ddb3646-ebda-11e7-8c3f-9a214cf093ae",
                  "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
              ]
          },
          "meta": {
              "name": "Bow",
              "description": "This is super awesome bow",
              "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
              "use": "item use",
              "category": "item category",
              "rarity": "rare",
              "origin": "won 1st place Doda GSL WW Turnament",
              "collection": "collection name",
              "series": {
                  "item": 5,
                  "of": 10
              }
          }
      }
      """
      And the response should be empty
      And the response status code should be 201
