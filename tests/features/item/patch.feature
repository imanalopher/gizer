Feature: Handle item via the RESTful API

  In order to sell items
  As a software owner
  I need to able to let users discovery items

  Background:
    Given I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"

  @createSchema @dropSchema
  Scenario: update item
    Given there are similar as following:
      | id                                   |
      | sf42d5a1-ebda-11e7-8c3f-9a214cf093ae |
      | dsdfd092-df54-345f-3rfd-343rsdffds3e |

    And there are Item as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    And the Item has Icons with the following details:
      | source                                  |
      | http://s3.amazonaws.com/gizer/icon1.jpg |
      | http://s3.amazonaws.com/gizer/icon2.jpg |
    And the Item has Images with the following details:
      | source                                   |
      | http://s3.amazonaws.com/gizer/image1.jpg |
      | http://s3.amazonaws.com/gizer/image2.jpg |
    And the Item has Videos with the following details:
      | source                            |
      | https://www.youtube.com/q=sdUdw83 |
    And the Item has model as "http://s3.amazonaws.com/gizer/models/bow.fbx"
    And the Item current status is "unlocked"
    And the Item current likes count is 123

    And the Item has meta as following:
      | name        | Bow                                      |
      | description | This is super awesome bow                |
      | wallet      | 7680adec8eabcabac676be9e83854ade0bd22cdb |
      | use         | item use                                 |
      | category    | item category                            |
      | rarity      | rare                                     |
      | origin      | won 1st place Doda GSL WW Turnament      |
      | released    | 2013-03-01T01:10:00+01:00                |
      | unlocked    | 2013-03-01T01:10:00+01:00                |
      | collection  | collection name                          |

    And the Item is 5 th of 10 in own series
    When I send a GET request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
        "id": "@string@",
        "resources": {
            "icons": [
                "http://s3.amazonaws.com/gizer/icon1.jpg",
                "http://s3.amazonaws.com/gizer/icon2.jpg"
            ],
            "images": [
                "http://s3.amazonaws.com/gizer/image1.jpg",
                "http://s3.amazonaws.com/gizer/image2.jpg"
            ],
            "videos": [
                "https://www.youtube.com/q=sdUdw83"
            ],
            "model": {
                "src": "http://s3.amazonaws.com/gizer/models/bow.fbx"
            }
        },
        "current": {
            "status": "unlocked",
            "likes": 123,
            "similar": []
        },
        "meta": {
            "name": "Bow",
            "description": "This is super awesome bow",
            "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
            "use": "item use",
            "category": "item category",
            "rarity": "rare",
            "origin": "won 1st place Doda GSL WW Turnament",
            "released": "2013-03-01T01:10:00+01:00",
            "unlocked": "2013-03-01T01:10:00+01:00",
            "collection": "collection name",
            "series": {
                "item": 5,
                "of": 10
            }
        }
    }
    """

    When I send a PATCH request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with body:
    """
    {
      "meta": {
        "name": "Updated Bow"
      },
      "current": {
        "similar": [
          "sf42d5a1-ebda-11e7-8c3f-9a214cf093ae",
          "dsdfd092-df54-345f-3rfd-343rsdffds3e"
        ]
      }
    }
    """
    And the response should be empty
    And the response status code should be 201

    When I send a GET request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
        "id": "@string@",
        "resources": {
            "icons": [
                "http://s3.amazonaws.com/gizer/icon1.jpg",
                "http://s3.amazonaws.com/gizer/icon2.jpg"
            ],
            "images": [
                "http://s3.amazonaws.com/gizer/image1.jpg",
                "http://s3.amazonaws.com/gizer/image2.jpg"
            ],
            "videos": [
                "https://www.youtube.com/q=sdUdw83"
            ],
            "model": {
                "src": "http://s3.amazonaws.com/gizer/models/bow.fbx"
            }
        },
        "current": {
            "status": "unlocked",
            "likes": 123,
            "similar": [
              "sf42d5a1-ebda-11e7-8c3f-9a214cf093ae",
              "dsdfd092-df54-345f-3rfd-343rsdffds3e"
            ]
        },
        "meta": {
            "name": "Updated Bow",
            "description": "This is super awesome bow",
            "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
            "use": "item use",
            "category": "item category",
            "rarity": "rare",
            "origin": "won 1st place Doda GSL WW Turnament",
            "released": "2013-03-01T01:10:00+01:00",
            "unlocked": "2013-03-01T01:10:00+01:00",
            "collection": "collection name",
            "series": {
                "item": 5,
                "of": 10
            }
        }
    }
    """

  @createSchema @dropSchema
  Scenario: update item - removing similar
    Given there are Item as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    And the Item has Icons with the following details:
      | source                                  |
      | http://s3.amazonaws.com/gizer/icon1.jpg |
      | http://s3.amazonaws.com/gizer/icon2.jpg |
    And the Item has Images with the following details:
      | source                                   |
      | http://s3.amazonaws.com/gizer/image1.jpg |
      | http://s3.amazonaws.com/gizer/image2.jpg |
    And the Item has Videos with the following details:
      | source                            |
      | https://www.youtube.com/q=sdUdw83 |
    And the Item has model as "http://s3.amazonaws.com/gizer/models/bow.fbx"
    And the Item current status is "unlocked"
    And the Item current likes count is 123
    And the Item current similar is as following:
      | id                                   |
      | f591fb15-737b-4b09-9523-f4521adc8f2e |
      | dsdfd092-df54-345f-3rfd-343rsdffds3e |

    And the Item has meta as following:
      | name        | Bow                                      |
      | description | This is super awesome bow                |
      | wallet      | 7680adec8eabcabac676be9e83854ade0bd22cdb |
      | use         | item use                                 |
      | category    | item category                            |
      | rarity      | rare                                     |
      | origin      | won 1st place Doda GSL WW Turnament      |
      | released    | 2013-03-01T01:10:00+01:00                |
      | unlocked    | 2013-03-01T01:10:00+01:00                |
      | collection  | collection name                          |

    And the Item is 5 th of 10 in own series
    When I send a GET request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
        "id": "@string@",
        "resources": {
            "icons": [
                "http://s3.amazonaws.com/gizer/icon1.jpg",
                "http://s3.amazonaws.com/gizer/icon2.jpg"
            ],
            "images": [
                "http://s3.amazonaws.com/gizer/image1.jpg",
                "http://s3.amazonaws.com/gizer/image2.jpg"
            ],
            "videos": [
                "https://www.youtube.com/q=sdUdw83"
            ],
            "model": {
                "src": "http://s3.amazonaws.com/gizer/models/bow.fbx"
            }
        },
        "current": {
            "status": "unlocked",
            "likes": 123,
            "similar": [
                "f591fb15-737b-4b09-9523-f4521adc8f2e",
                "dsdfd092-df54-345f-3rfd-343rsdffds3e"
            ]
        },
        "meta": {
            "name": "Bow",
            "description": "This is super awesome bow",
            "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
            "use": "item use",
            "category": "item category",
            "rarity": "rare",
            "origin": "won 1st place Doda GSL WW Turnament",
            "released": "2013-03-01T01:10:00+01:00",
            "unlocked": "2013-03-01T01:10:00+01:00",
            "collection": "collection name",
            "series": {
                "item": 5,
                "of": 10
            }
        }
    }
    """

    When I send a PATCH request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with body:
    """
    {
      "meta": {
        "name": "Updated Bow"
      },
      "current": {
        "similar": []
      }
    }
    """
    And the response should be empty
    And the response status code should be 201

    When I send a GET request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
        "id": "@string@",
        "resources": {
            "icons": [
                "http://s3.amazonaws.com/gizer/icon1.jpg",
                "http://s3.amazonaws.com/gizer/icon2.jpg"
            ],
            "images": [
                "http://s3.amazonaws.com/gizer/image1.jpg",
                "http://s3.amazonaws.com/gizer/image2.jpg"
            ],
            "videos": [
                "https://www.youtube.com/q=sdUdw83"
            ],
            "model": {
                "src": "http://s3.amazonaws.com/gizer/models/bow.fbx"
            }
        },
        "current": {
            "status": "unlocked",
            "likes": 123,
            "similar": []
        },
        "meta": {
            "name": "Updated Bow",
            "description": "This is super awesome bow",
            "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
            "use": "item use",
            "category": "item category",
            "rarity": "rare",
            "origin": "won 1st place Doda GSL WW Turnament",
            "released": "2013-03-01T01:10:00+01:00",
            "unlocked": "2013-03-01T01:10:00+01:00",
            "collection": "collection name",
            "series": {
                "item": 5,
                "of": 10
            }
        }
    }
    """

  @createSchema @dropSchema
  Scenario: update item - removing similar
    Given there are Item as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    And the Item has Icons with the following details:
      | source                                  |
      | http://s3.amazonaws.com/gizer/icon1.jpg |
      | http://s3.amazonaws.com/gizer/icon2.jpg |
    And the Item has Images with the following details:
      | source                                   |
      | http://s3.amazonaws.com/gizer/image1.jpg |
      | http://s3.amazonaws.com/gizer/image2.jpg |
    And the Item has Videos with the following details:
      | source                            |
      | https://www.youtube.com/q=sdUdw83 |
    And the Item has model as "http://s3.amazonaws.com/gizer/models/bow.fbx"
    And the Item current status is "unlocked"
    And the Item current likes count is 123
    And the Item current similar is as following:
      | id                                   |
      | agte3646-ebda-11e7-8c3f-9a214cf093ae |
      | dsdfd092-df54-345f-3rfd-343rsdffds3e |

    And the Item has meta as following:
      | name        | Bow                                      |
      | description | This is super awesome bow                |
      | wallet      | 7680adec8eabcabac676be9e83854ade0bd22cdb |
      | use         | item use                                 |
      | category    | item category                            |
      | rarity      | rare                                     |
      | origin      | won 1st place Doda GSL WW Turnament      |
      | released    | 2013-03-01T01:10:00+01:00                |
      | unlocked    | 2013-03-01T01:10:00+01:00                |
      | collection  | collection name                          |

    And the Item is 5 th of 10 in own series
    When I send a GET request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
        "id": "@string@",
        "resources": {
            "icons": [
                "http://s3.amazonaws.com/gizer/icon1.jpg",
                "http://s3.amazonaws.com/gizer/icon2.jpg"
            ],
            "images": [
                "http://s3.amazonaws.com/gizer/image1.jpg",
                "http://s3.amazonaws.com/gizer/image2.jpg"
            ],
            "videos": [
                "https://www.youtube.com/q=sdUdw83"
            ],
            "model": {
                "src": "http://s3.amazonaws.com/gizer/models/bow.fbx"
            }
        },
        "current": {
            "status": "unlocked",
            "likes": 123,
            "similar": [
              "agte3646-ebda-11e7-8c3f-9a214cf093ae",
              "dsdfd092-df54-345f-3rfd-343rsdffds3e"
            ]
        },
        "meta": {
            "name": "Bow",
            "description": "This is super awesome bow",
            "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
            "use": "item use",
            "category": "item category",
            "rarity": "rare",
            "origin": "won 1st place Doda GSL WW Turnament",
            "released": "2013-03-01T01:10:00+01:00",
            "unlocked": "2013-03-01T01:10:00+01:00",
            "collection": "collection name",
            "series": {
                "item": 5,
                "of": 10
            }
        }
    }
    """

    When I send a PATCH request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with body:
    """
    {
      "meta": {
        "name": "Updated Bow"
      },
      "current": {
        "similar": [
          "agte3646-ebda-11e7-8c3f-9a214cf093ae"
        ]
      }
    }
    """
    And the response should be empty
    And the response status code should be 201

    When I send a GET request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
        "id": "@string@",
        "resources": {
            "icons": [
                "http://s3.amazonaws.com/gizer/icon1.jpg",
                "http://s3.amazonaws.com/gizer/icon2.jpg"
            ],
            "images": [
                "http://s3.amazonaws.com/gizer/image1.jpg",
                "http://s3.amazonaws.com/gizer/image2.jpg"
            ],
            "videos": [
                "https://www.youtube.com/q=sdUdw83"
            ],
            "model": {
                "src": "http://s3.amazonaws.com/gizer/models/bow.fbx"
            }
        },
        "current": {
            "status": "unlocked",
            "likes": 123,
            "similar": [
              "agte3646-ebda-11e7-8c3f-9a214cf093ae"
            ]
        },
        "meta": {
            "name": "Updated Bow",
            "description": "This is super awesome bow",
            "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
            "use": "item use",
            "category": "item category",
            "rarity": "rare",
            "origin": "won 1st place Doda GSL WW Turnament",
            "released": "2013-03-01T01:10:00+01:00",
            "unlocked": "2013-03-01T01:10:00+01:00",
            "collection": "collection name",
            "series": {
                "item": 5,
                "of": 10
            }
        }
    }
    """

  @createSchema @dropSchema
  Scenario: update item - trying modify released date, unlocked date, status and likes
    Given there are Item as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"

    And the Item has Icons with the following details:
      | source                                  |
      | http://s3.amazonaws.com/gizer/icon1.jpg |
      | http://s3.amazonaws.com/gizer/icon2.jpg |
    And the Item has Images with the following details:
      | source                                   |
      | http://s3.amazonaws.com/gizer/image1.jpg |
      | http://s3.amazonaws.com/gizer/image2.jpg |
    And the Item has Videos with the following details:
      | source                            |
      | https://www.youtube.com/q=sdUdw83 |
    And the Item has model as "http://s3.amazonaws.com/gizer/models/bow.fbx"
    And the Item current status is "locked"
    And the Item current likes count is 123
    And the Item current similar is as following:
      | id                                   |
      | f591fb15-737b-4b09-9523-f4521adc8f2e |
      | dsdfd092-df54-345f-3rfd-343rsdffds3e |

    And the Item has meta as following:
      | name        | Bow                                      |
      | description | This is super awesome bow                |
      | wallet      | 7680adec8eabcabac676be9e83854ade0bd22cdb |
      | use         | item use                                 |
      | category    | item category                            |
      | rarity      | rare                                     |
      | origin      | won 1st place Doda GSL WW Turnament      |
      | released    | 2013-03-01T01:10:00+01:00                |
      | unlocked    | 2013-03-01T01:10:00+01:00                |
      | collection  | collection name                          |

    And the Item is 5 th of 10 in own series

    When I send a PATCH request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with body:
    """
    {
      "meta": {
        "released": "2018-01-19T00:00:00+01:00",
        "unlocked": "2018-01-19T00:10:00+01:00"
      },
      "current": {
        "status": "unlocked",
        "likes": 250
      }
    }
    """
    And the response should be empty
    And the response status code should be 201

    When I send a GET request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
        "id": "@string@",
        "resources": {
            "icons": [
                "http://s3.amazonaws.com/gizer/icon1.jpg",
                "http://s3.amazonaws.com/gizer/icon2.jpg"
            ],
            "images": [
                "http://s3.amazonaws.com/gizer/image1.jpg",
                "http://s3.amazonaws.com/gizer/image2.jpg"
            ],
            "videos": [
                "https://www.youtube.com/q=sdUdw83"
            ],
            "model": {
                "src": "http://s3.amazonaws.com/gizer/models/bow.fbx"
            }
        },
        "current": {
            "status": "locked",
            "likes": 123,
            "similar": [
                "f591fb15-737b-4b09-9523-f4521adc8f2e",
                "dsdfd092-df54-345f-3rfd-343rsdffds3e"
            ]
        },
        "meta": {
            "name": "Bow",
            "description": "This is super awesome bow",
            "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
            "use": "item use",
            "category": "item category",
            "rarity": "rare",
            "origin": "won 1st place Doda GSL WW Turnament",
            "released": "2013-03-01T01:10:00+01:00",
            "unlocked": "2013-03-01T01:10:00+01:00",
            "collection": "collection name",
            "series": {
                "item": 5,
                "of": 10
            }
        }
    }
    """

  @createSchema @dropSchema
  Scenario: update item - update resource

    Given there are Icons with the following details:
      | source                                  |
      | http://s3.amazonaws.com/gizer/icon1.jpg |
      | http://s3.amazonaws.com/gizer/icon2.jpg |
    And there are Images with the following details:
      | source                                   |
      | http://s3.amazonaws.com/gizer/image1.jpg |
      | http://s3.amazonaws.com/gizer/image2.jpg |

    And there are Item as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"

    And the Item has Videos with the following details:
      | source                            |
      | https://www.youtube.com/q=sdUdw83 |
    And the Item has model as "http://s3.amazonaws.com/gizer/models/bow.fbx"
    And the Item current status is "locked"
    And the Item current likes count is 123
    And the Item current similar is as following:
      | id                                   |
      | f591fb15-737b-4b09-9523-f4521adc8f2e |
      | dsdfd092-df54-345f-3rfd-343rsdffds3e |

    And the Item has meta as following:
      | name        | Bow                                      |
      | description | This is super awesome bow                |
      | wallet      | 7680adec8eabcabac676be9e83854ade0bd22cdb |
      | use         | item use                                 |
      | category    | item category                            |
      | rarity      | rare                                     |
      | origin      | won 1st place Doda GSL WW Turnament      |
      | released    | 2013-03-01T01:10:00+01:00                |
      | unlocked    | 2013-03-01T01:10:00+01:00                |
      | collection  | collection name                          |

    And the Item is 5 th of 10 in own series

    When I send a PATCH request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with body:
    """
    {
      "resources": {
        "icons": [
            "http://s3.amazonaws.com/gizer/icon1.jpg",
            "http://s3.amazonaws.com/gizer/icon2.jpg"
        ],
        "images": [
            "http://s3.amazonaws.com/gizer/image1.jpg",
            "http://s3.amazonaws.com/gizer/image2.jpg"
        ],
        "videos": [
            "https://www.youtube.com/q=lkaidf"
        ],
        "model": {
            "src": "http://s3.amazonaws.com/gizer/models/newbow.fbx"
        }
      },
      "meta": {
          "name": "Awesome Shield",
          "description": "This is super awesome shield",
          "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
          "use": "use with bow",
          "category": "shield category",
          "rarity": "normal",
          "origin": "3st place Doda GSL WW Turnament",
          "collection": "shield collection name",
          "series": {
              "item": 599,
              "of": 1000
          }
      }
    }
    """
    And the response should be empty
    And the response status code should be 201

    When I send a GET request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
      "id": "@string@",
      "resources": {
          "icons": [
              "http://s3.amazonaws.com/gizer/icon1.jpg",
              "http://s3.amazonaws.com/gizer/icon2.jpg"
          ],
          "images": [
              "http://s3.amazonaws.com/gizer/image1.jpg",
              "http://s3.amazonaws.com/gizer/image2.jpg"
          ],
          "videos": [
              "https://www.youtube.com/q=lkaidf"
          ],
          "model": {
              "src": "http://s3.amazonaws.com/gizer/models/newbow.fbx"
          }
      },
      "current": {
          "status": "locked",
          "likes": 123,
          "similar": [
              "f591fb15-737b-4b09-9523-f4521adc8f2e",
              "dsdfd092-df54-345f-3rfd-343rsdffds3e"
          ]
      },
      "meta": {
          "name": "Awesome Shield",
          "description": "This is super awesome shield",
          "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
          "use": "use with bow",
          "category": "shield category",
          "rarity": "normal",
          "origin": "3st place Doda GSL WW Turnament",
          "released": "2013-03-01T01:10:00+01:00",
          "unlocked": "2013-03-01T01:10:00+01:00",
          "collection": "shield collection name",
          "series": {
              "item": 599,
              "of": 1000
          }
      }
    }
    """

  @createSchema @dropSchema
  Scenario: update item - remove all relations with resource
    Given there are Item as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    And the Item has Icons with the following details:
      | source                                  |
      | http://s3.amazonaws.com/gizer/icon1.jpg |
      | http://s3.amazonaws.com/gizer/icon2.jpg |
    And the Item has Images with the following details:
      | source                                   |
      | http://s3.amazonaws.com/gizer/image1.jpg |
      | http://s3.amazonaws.com/gizer/image2.jpg |

    And the Item has Videos with the following details:
      | source                            |
      | https://www.youtube.com/q=sdUdw83 |
    And the Item has model as "http://s3.amazonaws.com/gizer/models/bow.fbx"
    And the Item current status is "locked"
    And the Item current likes count is 123
    And the Item current similar is as following:
      | id                                   |
      | f591fb15-737b-4b09-9523-f4521adc8f2e |
      | dsdfd092-df54-345f-3rfd-343rsdffds3e |

    And the Item has meta as following:
      | name        | Bow                                      |
      | description | This is super awesome bow                |
      | wallet      | 7680adec8eabcabac676be9e83854ade0bd22cdb |
      | use         | item use                                 |
      | category    | item category                            |
      | rarity      | rare                                     |
      | origin      | won 1st place Doda GSL WW Turnament      |
      | released    | 2013-03-01T01:10:00+01:00                |
      | unlocked    | 2013-03-01T01:10:00+01:00                |
      | collection  | collection name                          |

    And the Item is 5 th of 10 in own series

    When I send a GET request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
      "id": "@string@",
      "resources": {
          "icons": [
            "http://s3.amazonaws.com/gizer/icon1.jpg",
            "http://s3.amazonaws.com/gizer/icon2.jpg"
          ],
          "images": [
            "http://s3.amazonaws.com/gizer/image1.jpg",
            "http://s3.amazonaws.com/gizer/image2.jpg"
          ],
          "videos": [
              "https://www.youtube.com/q=sdUdw83"
          ],
          "model": {
              "src": "http://s3.amazonaws.com/gizer/models/bow.fbx"
          }
      },
      "current": {
          "status": "locked",
          "likes": 123,
          "similar": [
              "f591fb15-737b-4b09-9523-f4521adc8f2e",
              "dsdfd092-df54-345f-3rfd-343rsdffds3e"
          ]
      },
      "meta": {
          "name": "Bow",
          "description": "This is super awesome bow",
          "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
          "use": "item use",
          "category": "item category",
          "rarity": "rare",
          "origin": "won 1st place Doda GSL WW Turnament",
          "released": "2013-03-01T01:10:00+01:00",
          "unlocked": "2013-03-01T01:10:00+01:00",
          "collection": "collection name",
          "series": {
              "item": 5,
              "of": 10
          }
      }
    }
    """

    When I send a PATCH request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with body:
    """
    {
      "resources": {
        "icons": [],
        "images": [],
        "videos": [
            "https://www.youtube.com/q=lkaidf"
        ],
        "model": {
            "src": "http://s3.amazonaws.com/gizer/models/newbow.fbx"
        }
      },
      "meta": {
          "name": "Awesome Shield",
          "description": "This is super awesome shield",
          "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
          "use": "use with bow",
          "category": "shield category",
          "rarity": "normal",
          "origin": "3st place Doda GSL WW Turnament",
          "collection": "shield collection name",
          "series": {
              "item": 599,
              "of": 1000
          }
      }
    }
    """
    Then the response should be empty
    And the response status code should be 201

    When I send a GET request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
      "id": "@string@",
      "resources": {
          "icons": [],
          "images": [],
          "videos": [
              "https://www.youtube.com/q=lkaidf"
          ],
          "model": {
              "src": "http://s3.amazonaws.com/gizer/models/newbow.fbx"
          }
      },
      "current": {
          "status": "locked",
          "likes": 123,
          "similar": [
              "f591fb15-737b-4b09-9523-f4521adc8f2e",
              "dsdfd092-df54-345f-3rfd-343rsdffds3e"
          ]
      },
      "meta": {
          "name": "Awesome Shield",
          "description": "This is super awesome shield",
          "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
          "use": "use with bow",
          "category": "shield category",
          "rarity": "normal",
          "origin": "3st place Doda GSL WW Turnament",
          "released": "2013-03-01T01:10:00+01:00",
          "unlocked": "2013-03-01T01:10:00+01:00",
          "collection": "shield collection name",
          "series": {
              "item": 599,
              "of": 1000
          }
      }
    }
    """

  @createSchema @dropSchema
  Scenario: update item - partially remove relations with resource
    Given there are Item as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    And the Item has Icons with the following details:
      | source                                  |
      | http://s3.amazonaws.com/gizer/icon1.jpg |
      | http://s3.amazonaws.com/gizer/icon2.jpg |
    And the Item has Images with the following details:
      | source                                   |
      | http://s3.amazonaws.com/gizer/image1.jpg |
      | http://s3.amazonaws.com/gizer/image2.jpg |

    And the Item has Videos with the following details:
      | source                            |
      | https://www.youtube.com/q=sdUdw83 |
    And the Item has model as "http://s3.amazonaws.com/gizer/models/bow.fbx"
    And the Item current status is "locked"
    And the Item current likes count is 123
    And the Item current similar is as following:
      | id                                   |
      | f591fb15-737b-4b09-9523-f4521adc8f2e |
      | dsdfd092-df54-345f-3rfd-343rsdffds3e |

    And the Item has meta as following:
      | name        | Bow                                      |
      | description | This is super awesome bow                |
      | wallet      | 7680adec8eabcabac676be9e83854ade0bd22cdb |
      | use         | item use                                 |
      | category    | item category                            |
      | rarity      | rare                                     |
      | origin      | won 1st place Doda GSL WW Turnament      |
      | released    | 2013-03-01T01:10:00+01:00                |
      | unlocked    | 2013-03-01T01:10:00+01:00                |
      | collection  | collection name                          |

    And the Item is 5 th of 10 in own series

    When I send a GET request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
      "id": "@string@",
      "resources": {
          "icons": [
            "http://s3.amazonaws.com/gizer/icon1.jpg",
            "http://s3.amazonaws.com/gizer/icon2.jpg"
          ],
          "images": [
            "http://s3.amazonaws.com/gizer/image1.jpg",
            "http://s3.amazonaws.com/gizer/image2.jpg"
          ],
          "videos": [
              "https://www.youtube.com/q=sdUdw83"
          ],
          "model": {
              "src": "http://s3.amazonaws.com/gizer/models/bow.fbx"
          }
      },
      "current": {
          "status": "locked",
          "likes": 123,
          "similar": [
              "f591fb15-737b-4b09-9523-f4521adc8f2e",
              "dsdfd092-df54-345f-3rfd-343rsdffds3e"
          ]
      },
      "meta": {
          "name": "Bow",
          "description": "This is super awesome bow",
          "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
          "use": "item use",
          "category": "item category",
          "rarity": "rare",
          "origin": "won 1st place Doda GSL WW Turnament",
          "released": "2013-03-01T01:10:00+01:00",
          "unlocked": "2013-03-01T01:10:00+01:00",
          "collection": "collection name",
          "series": {
              "item": 5,
              "of": 10
          }
      }
    }
    """

    When I send a PATCH request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with body:
    """
    {
      "resources": {
        "icons": [
          "http://s3.amazonaws.com/gizer/icon1.jpg"
        ],
        "images": [
          "http://s3.amazonaws.com/gizer/image2.jpg"
        ],
        "videos": [
            "https://www.youtube.com/q=lkaidf"
        ],
        "model": {
            "src": "http://s3.amazonaws.com/gizer/models/newbow.fbx"
        }
      },
      "meta": {
          "name": "Awesome Shield",
          "description": "This is super awesome shield",
          "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
          "use": "use with bow",
          "category": "shield category",
          "rarity": "normal",
          "origin": "3st place Doda GSL WW Turnament",
          "collection": "shield collection name",
          "series": {
              "item": 599,
              "of": 1000
          }
      }
    }
    """
    Then the response should be empty
    And the response status code should be 201

    When I send a GET request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
      "id": "@string@",
      "resources": {
          "icons": [
            "http://s3.amazonaws.com/gizer/icon1.jpg"
          ],
          "images": [
            "http://s3.amazonaws.com/gizer/image2.jpg"
          ],
          "videos": [
              "https://www.youtube.com/q=lkaidf"
          ],
          "model": {
              "src": "http://s3.amazonaws.com/gizer/models/newbow.fbx"
          }
      },
      "current": {
          "status": "locked",
          "likes": 123,
          "similar": [
              "f591fb15-737b-4b09-9523-f4521adc8f2e",
              "dsdfd092-df54-345f-3rfd-343rsdffds3e"
          ]
      },
      "meta": {
          "name": "Awesome Shield",
          "description": "This is super awesome shield",
          "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
          "use": "use with bow",
          "category": "shield category",
          "rarity": "normal",
          "origin": "3st place Doda GSL WW Turnament",
          "released": "2013-03-01T01:10:00+01:00",
          "unlocked": "2013-03-01T01:10:00+01:00",
          "collection": "shield collection name",
          "series": {
              "item": 599,
              "of": 1000
          }
      }
    }
    """

  @createSchema @dropSchema
  Scenario: update item - without icons, images
    Given there are Item as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    And the Item has Icons with the following details:
      | source                                  |
      | http://s3.amazonaws.com/gizer/icon1.jpg |
      | http://s3.amazonaws.com/gizer/icon2.jpg |
    And the Item has Images with the following details:
      | source                                   |
      | http://s3.amazonaws.com/gizer/image1.jpg |
      | http://s3.amazonaws.com/gizer/image2.jpg |

    And the Item has Videos with the following details:
      | source                            |
      | https://www.youtube.com/q=sdUdw83 |
    And the Item has model as "http://s3.amazonaws.com/gizer/models/bow.fbx"
    And the Item current status is "locked"
    And the Item current likes count is 123
    And the Item current similar is as following:
      | id                                   |
      | f591fb15-737b-4b09-9523-f4521adc8f2e |
      | dsdfd092-df54-345f-3rfd-343rsdffds3e |

    And the Item has meta as following:
      | name        | Bow                                      |
      | description | This is super awesome bow                |
      | wallet      | 7680adec8eabcabac676be9e83854ade0bd22cdb |
      | use         | item use                                 |
      | category    | item category                            |
      | rarity      | rare                                     |
      | origin      | won 1st place Doda GSL WW Turnament      |
      | released    | 2013-03-01T01:10:00+01:00                |
      | unlocked    | 2013-03-01T01:10:00+01:00                |
      | collection  | collection name                          |

    And the Item is 5 th of 10 in own series

    When I send a GET request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
      "id": "@string@",
      "resources": {
          "icons": [
            "http://s3.amazonaws.com/gizer/icon1.jpg",
            "http://s3.amazonaws.com/gizer/icon2.jpg"
          ],
          "images": [
            "http://s3.amazonaws.com/gizer/image1.jpg",
            "http://s3.amazonaws.com/gizer/image2.jpg"
          ],
          "videos": [
              "https://www.youtube.com/q=sdUdw83"
          ],
          "model": {
              "src": "http://s3.amazonaws.com/gizer/models/bow.fbx"
          }
      },
      "current": {
          "status": "locked",
          "likes": 123,
          "similar": [
              "f591fb15-737b-4b09-9523-f4521adc8f2e",
              "dsdfd092-df54-345f-3rfd-343rsdffds3e"
          ]
      },
      "meta": {
          "name": "Bow",
          "description": "This is super awesome bow",
          "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
          "use": "item use",
          "category": "item category",
          "rarity": "rare",
          "origin": "won 1st place Doda GSL WW Turnament",
          "released": "2013-03-01T01:10:00+01:00",
          "unlocked": "2013-03-01T01:10:00+01:00",
          "collection": "collection name",
          "series": {
              "item": 5,
              "of": 10
          }
      }
    }
    """

    When I send a PATCH request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with body:
    """
    {
      "resources": {
        "videos": [
            "https://www.youtube.com/q=lkaidf"
        ],
        "model": {
            "src": "http://s3.amazonaws.com/gizer/models/newbow.fbx"
        }
      },
      "meta": {
          "name": "Awesome Shield",
          "description": "This is super awesome shield",
          "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
          "use": "use with bow",
          "category": "shield category",
          "rarity": "normal",
          "origin": "3st place Doda GSL WW Turnament",
          "collection": "shield collection name",
          "series": {
              "item": 599,
              "of": 1000
          }
      }
    }
    """
    Then the response should be empty
    And the response status code should be 201

    When I send a GET request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
      "id": "@string@",
      "resources": {
          "icons": [
            "http://s3.amazonaws.com/gizer/icon1.jpg",
            "http://s3.amazonaws.com/gizer/icon2.jpg"
          ],
          "images": [
            "http://s3.amazonaws.com/gizer/image1.jpg",
            "http://s3.amazonaws.com/gizer/image2.jpg"
          ],
          "videos": [
              "https://www.youtube.com/q=lkaidf"
          ],
          "model": {
              "src": "http://s3.amazonaws.com/gizer/models/newbow.fbx"
          }
      },
      "current": {
          "status": "locked",
          "likes": 123,
          "similar": [
              "f591fb15-737b-4b09-9523-f4521adc8f2e",
              "dsdfd092-df54-345f-3rfd-343rsdffds3e"
          ]
      },
      "meta": {
          "name": "Awesome Shield",
          "description": "This is super awesome shield",
          "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
          "use": "use with bow",
          "category": "shield category",
          "rarity": "normal",
          "origin": "3st place Doda GSL WW Turnament",
          "released": "2013-03-01T01:10:00+01:00",
          "unlocked": "2013-03-01T01:10:00+01:00",
          "collection": "shield collection name",
          "series": {
              "item": 599,
              "of": 1000
          }
      }
    }
    """
