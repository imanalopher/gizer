Feature: Handle item via the RESTful API

  In order to sell items
  As a software owner
  I need to able to let users discovery items

  Background:
    Given I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"

  @createSchema @dropSchema
  Scenario: get items
    Given there are Item as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"

    And the Item has Icons with the following details:
      | source                                  |
      | http://s3.amazonaws.com/gizer/icon1.jpg |
      | http://s3.amazonaws.com/gizer/icon2.jpg |
    And the Item has Images with the following details:
      | source                                   |
      | http://s3.amazonaws.com/gizer/image1.jpg |
      | http://s3.amazonaws.com/gizer/image2.jpg |
    And the Item has Videos with the following details:
      | source                            |
      | https://www.youtube.com/q=sdUdw83 |
    And the Item has model as "http://s3.amazonaws.com/gizer/models/bow.fbx"
    And the Item current status is "unlocked"
    And the Item current likes count is 123
    And the Item has meta as following:
      | name        | Bow                                      |
      | description | This is super awesome bow                |
      | wallet      | 7680adec8eabcabac676be9e83854ade0bd22cdb |
      | use         | item use                                 |
      | category    | item category                            |
      | rarity      | rare                                     |
      | origin      | won 1st place Doda GSL WW Turnament      |
      | released    | 2013-03-01T01:10:00+01:00                |
      | unlocked    | 2013-03-01T01:10:00+01:00                |
      | collection  | collection name                          |

    And the Item is 5 th of 10 in own series

    When I send a GET request to "/item"
    Then the JSON should match pattern:
    """
     [ {
        "id": "@string@",
        "resources": {
            "icons": [
                "http://s3.amazonaws.com/gizer/icon1.jpg",
                "http://s3.amazonaws.com/gizer/icon2.jpg"
            ],
            "images": [
                "http://s3.amazonaws.com/gizer/image1.jpg",
                "http://s3.amazonaws.com/gizer/image2.jpg"
            ],
            "videos": [
                "https://www.youtube.com/q=sdUdw83"
            ],
            "model": {
                "src": "http://s3.amazonaws.com/gizer/models/bow.fbx"
            }
        },
        "current": {
            "status": "unlocked",
            "likes": 123,
            "similar": []
        },
        "meta": {
            "name": "Bow",
            "description": "This is super awesome bow",
            "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
            "use": "item use",
            "category": "item category",
            "rarity": "rare",
            "origin": "won 1st place Doda GSL WW Turnament",
            "released": "2013-03-01T01:10:00+01:00",
            "unlocked": "2013-03-01T01:10:00+01:00",
            "collection": "collection name",
            "series": {
                "item": 5,
                "of": 10
            }
        }
    } ]
    """