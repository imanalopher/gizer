Feature: Handle item via the RESTful API

  In order to sell items
  As a software owner
  I need to able to let users discovery items

  Background:
    Given I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"

  @createSchema @dropSchema
  Scenario: get items
    Given there are Item as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"

    And the Item has Icons with the following details:
      | source                                  |
      | http://s3.amazonaws.com/gizer/icon1.jpg |
      | http://s3.amazonaws.com/gizer/icon2.jpg |
    And the Item has Images with the following details:
      | source                                   |
      | http://s3.amazonaws.com/gizer/image1.jpg |
      | http://s3.amazonaws.com/gizer/image2.jpg |
    And the Item has Videos with the following details:
      | source                            |
      | https://www.youtube.com/q=sdUdw83 |
    And the Item has model as "http://s3.amazonaws.com/gizer/models/bow.fbx"
    And the Item current status is "unlocked"
    And the Item current likes count is 123
    And the Item current similar is as following:
      | id                                   |
      | f591fb15-737b-4b09-9523-f4521adc8f2e |
      | dsdfd092-df54-345f-3rfd-343rsdffds3e |

    And the Item has meta as following:
      | name        | Bow                                      |
      | description | This is super awesome bow                |
      | wallet      | 7680adec8eabcabac676be9e83854ade0bd22cdb |
      | use         | item use                                 |
      | category    | item category                            |
      | rarity      | rare                                     |
      | origin      | won 1st place Doda GSL WW Turnament      |
      | released    | 2013-03-01T01:10:00+01:00                |
      | unlocked    | 2013-03-01T01:10:00+01:00                |
      | collection  | collection name                          |

    And the Item is 5 th of 10 in own series

    When I send a GET request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
        "id": "@string@",
        "resources": {
            "icons": [
                "http://s3.amazonaws.com/gizer/icon1.jpg",
                "http://s3.amazonaws.com/gizer/icon2.jpg"
            ],
            "images": [
                "http://s3.amazonaws.com/gizer/image1.jpg",
                "http://s3.amazonaws.com/gizer/image2.jpg"
            ],
            "videos": [
                "https://www.youtube.com/q=sdUdw83"
            ],
            "model": {
                "src": "http://s3.amazonaws.com/gizer/models/bow.fbx"
            }
        },
        "current": {
            "status": "unlocked",
            "likes": 123,
            "similar": [
                "f591fb15-737b-4b09-9523-f4521adc8f2e",
                "dsdfd092-df54-345f-3rfd-343rsdffds3e"
            ]
        },
        "meta": {
            "name": "Bow",
            "description": "This is super awesome bow",
            "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
            "use": "item use",
            "category": "item category",
            "rarity": "rare",
            "origin": "won 1st place Doda GSL WW Turnament",
            "released": "2013-03-01T01:10:00+01:00",
            "unlocked": "2013-03-01T01:10:00+01:00",
            "collection": "collection name",
            "series": {
                "item": 5,
                "of": 10
            }
        }
    }
    """

  @createSchema @dropSchema
  Scenario: get items - one more time
    Given there are Item as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"

    And the Item has Icons with the following details:
      | source                                   |
      | http://s3.amazonaws.com/gizer/icon21.jpg |
      | http://s3.amazonaws.com/gizer/icon22.jpg |
    And the Item has Images with the following details:
      | source                                   |
      | http://s3.amazonaws.com/gizer/image21.jpg |
      | http://s3.amazonaws.com/gizer/image22.jpg |
    And the Item has Videos with the following details:
      | source                            |
      | https://www.youtube.com/q=sdUdw83 |
    And the Item has model as "http://s3.amazonaws.com/gizer/models/bow2.fbx"
    And the Item current status is "locked"
    And the Item current likes count is 123
    And the Item current similar is as following:
      | id                                   |
      | f591fb15-737b-4b09-9523-f4521adc8f2e |
      | dsdfd092-df54-345f-3rfd-343rsdffds3e |

    And the Item has meta as following:
      | name        | Bow2                                     |
      | description | This is super awesome bow2               |
      | wallet      | 7680adec8eabcabac676be9e83854ade0bd22cdb |
      | use         | item use                                 |
      | category    | item category                            |
      | rarity      | rare                                     |
      | origin      | won 1st place Doda GSL WW Turnament      |
      | released    | 2013-03-01T01:10:00+01:00                |
      | unlocked    | 2013-03-01T01:10:00+01:00                |
      | collection  | collection name                          |

    And the Item is 8 th of 10 in own series

    When I send a GET request to "/item/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
        "id": "@string@",
        "resources": {
            "icons": [
                "http://s3.amazonaws.com/gizer/icon21.jpg",
                "http://s3.amazonaws.com/gizer/icon22.jpg"
            ],
            "images": [
                "http://s3.amazonaws.com/gizer/image21.jpg",
                "http://s3.amazonaws.com/gizer/image22.jpg"
            ],
            "videos": [
                "https://www.youtube.com/q=sdUdw83"
            ],
            "model": {
                "src": "http://s3.amazonaws.com/gizer/models/bow2.fbx"
            }
        },
        "current": {
            "status": "locked",
            "likes": 123,
            "similar": [
                "f591fb15-737b-4b09-9523-f4521adc8f2e",
                "dsdfd092-df54-345f-3rfd-343rsdffds3e"
            ]
        },
        "meta": {
            "name": "Bow2",
            "description": "This is super awesome bow2",
            "wallet": "7680adec8eabcabac676be9e83854ade0bd22cdb",
            "use": "item use",
            "category": "item category",
            "rarity": "rare",
            "origin": "won 1st place Doda GSL WW Turnament",
            "released": "2013-03-01T01:10:00+01:00",
            "unlocked": "2013-03-01T01:10:00+01:00",
            "collection": "collection name",
            "series": {
                "item": 8,
                "of": 10
            }
        }
    }
    """