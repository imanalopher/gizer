Feature: Handle item via the RESTful API

  In order to sell items
  As a software owner
  I need to able to let management users

  Background:
    Given I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"

  @createSchema @dropSchema
  Scenario: create user
      When I send a "PUT" request to "/user" with body:
      """
      {
          "nick": "Johny Bravo",
          "email": "johny@bravo.io",
          "type": "user",
          "username": "jbravo",
          "password": "hashed",
          "active": 1,
          "address": {
              "street": "7th & 45th, Manhattan",
              "city": "NYC",
              "country": "USA"
          },
          "about": "I am a hardcore player!",
          "reputation":21000,
          "timezone": "UTC",
          "interests": [
              "Fishing",
              "Bowling",
              "Dodgeball"
          ],
          "gzr": {
              "type": "wallet",
              "id": "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "amount":130
          },
          "boosts":25200,
          "avatar": {
              "type": "2d",
              "uri": "http://s3.amazonaws.com/gizer/avatars/clown.jpg"
          },
          "social": [
              {
                  "name": "google+",
                  "url": "https://plus.google.com/11730245375828271109"
              },
              {
                  "name": "facebook",
                  "url": "https://www.facebook.com/jbravo"
              }
          ],
          "knows": [
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw"
          ],
          "owns": [
              "item/1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "item/1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "item/1dab3646-e2da-01e7-2c3a-1a294xa092aw"
          ]
      }
      """
      Then the response should be empty
      And the response status code should be 201

  @createSchema @dropSchema
  Scenario: create user - minimal data
      When I send a "PUT" request to "/user" with body:
      """
      {
          "nick": "Johny Bravo",
          "email": "johny@bravo.io",
          "gzr": {
              "type": "wallet",
              "id": "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "amount":130
          }
      }
      """
      Then the response should be empty
      And the response status code should be 201
      When I send a "GET" request to "/user?q=John"
      Then the JSON should match pattern:
      """
      [
        {
          "id": "@string@",
          "nick": "Johny Bravo",
          "email": "johny@bravo.io",
          "type": null,
          "username": null,
          "active": null,
          "address": {
              "street": null,
              "city": null,
              "country": null
          },
          "about": null,
          "reputation": null,
          "timezone": null,
          "interests": [],
          "gzr": {
              "type": "wallet",
              "id": "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "amount":130
          },
          "boosts": null,
          "avatar": {
              "type": null,
              "uri": null
          },
          "social": [],
          "knows": [],
          "owns": [],
          "transactions": [],
          "created_at": "@string@.isDateTime()"
        }
      ]
      """

  @createSchema @dropSchema
  Scenario: create user - the same wallet ID
    When I send a "PUT" request to "/user" with body:
      """
      {
          "nick": "Johny Bravo",
          "email": "johny@bravo.io",
          "type": "user",
          "username": "jbravo",
          "password": "hashed",
          "active": 1,
          "address": {
              "street": "7th & 45th, Manhattan",
              "city": "NYC",
              "country": "USA"
          },
          "about": "I am a hardcore player!",
          "reputation":21000,
          "timezone": "UTC",
          "interests": [
              "Fishing",
              "Bowling",
              "Dodgeball"
          ],
          "gzr": {
              "type": "wallet",
              "id": "ef4419b4-509f-4f6c-9e8a-2fda706d7d48",
              "amount":130
          },
          "boosts":25200,
          "avatar": {
              "type": "2d",
              "uri": "http://s3.amazonaws.com/gizer/avatars/clown.jpg"
          },
          "social": [
              {
                  "name": "google+",
                  "url": "https://plus.google.com/11730245375828271109"
              },
              {
                  "name": "facebook",
                  "url": "https://www.facebook.com/jbravo"
              }
          ],
          "knows": [
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw"
          ],
          "owns": [
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw"
          ]
      }
      """
    Then the response status code should be 201
    And the response should be empty

    Given I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"
    When I send a "PUT" request to "/user" with body:
      """
      {
          "nick": "Johny Bravo",
          "email": "johny@bravo.io",
          "type": "user",
          "username": "jbravo",
          "password": "hashed",
          "active": 1,
          "address": {
              "street": "7th & 45th, Manhattan",
              "city": "NYC",
              "country": "USA"
          },
          "about": "I am a hardcore player!",
          "reputation":21000,
          "timezone": "UTC",
          "interests": [
              "Fishing",
              "Bowling",
              "Dodgeball"
          ],
          "gzr": {
              "type": "wallet",
              "id": "ef4419b4-509f-4f6c-9e8a-2fda706d7d48",
              "amount":130
          },
          "boosts":25200,
          "avatar": {
              "type": "2d",
              "uri": "http://s3.amazonaws.com/gizer/avatars/clown.jpg"
          },
          "social": [
              {
                  "name": "google+",
                  "url": "https://plus.google.com/11730245375828271109"
              },
              {
                  "name": "facebook",
                  "url": "https://www.facebook.com/jbravo"
              }
          ],
          "knows": [
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "1dab3646-e2da-01e7-2c3a-1a294xa092aw"
          ],
          "owns": [
              "item/1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "item/1dab3646-e2da-01e7-2c3a-1a294xa092aw",
              "item/1dab3646-e2da-01e7-2c3a-1a294xa092aw"
          ]
      }
      """
    Then the response status code should be 400
    And the JSON should match pattern:
      """
      {
          "code": 400,
          "message": "User with wallet ID ef4419b4-509f-4f6c-9e8a-2fda706d7d48 is already registered"
      }
      """