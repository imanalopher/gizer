Feature: Handle item via the RESTful API

  In order to sell items
  As a software owner
  I need to able to let management users

  Background:
    Given I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"

  @createSchema @dropSchema
  Scenario: update user
    Given there are Users as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with the following details:
      | username  | email          | password |
      | jbravo    | johny@bravo.io | hashed   |
    And the User details are as following:
      | nick | Johny Bravo |
      | type | user |
      | active | 1 |
      | about | I am a hardcore player! |
      | reputation | 21000 |
      | timezone | UTC |
      | boosts | 25200 |
    And the User has Address as following:
      | street                | city | country |
      | 7th & 45th, Manhattan | NYC  | USA     |
    And the User has interests as following:
      | name       |
      | Fishing    |
      | Bowling    |
      | Dodgeball  |
    And the User has Gzr as following:
      | type   | id                                   | amount |
      | wallet | 1dab3646-e2da-01e7-2c3a-1a294xa092aw | 130    |
    And the User has Avatar as following:
      | type | uri                                             |
      | 2d   | http://s3.amazonaws.com/gizer/avatars/clown.jpg |
      And the User has Social as following:
      | name     | url                                          |
      | google+  | https://plus.google.com/11730245375828271109 |
      | facebook | https://www.facebook.com/jbravo              |
    And the User knows:
      | id                                   |
      | 1dab3646-e2da-01e7-2c3a-1a294xa092aw |
      | 1dab3646-e2da-01e7-2c3a-123ewdfsa2r5 |
      | dasddadd-e2da-01e7-2c3a-123ewdfsa2r5 |
      | dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf |
      | zcnw2ss2-dsdd-ssds-d332-321asdasfs2x |
    And the User owns:
      | id                                        |
      | item/1dab3646-e2da-01e7-2c3a-1a294xa092aw |
      | item/dasddadd-e2da-01e7-2c3a-123ewdfsa2r5 |
      | item/dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf |

    When I send a "PATCH" request to "/user/4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with body:
    """
    {
      "nick": "Super Mike",
      "owns": [
        "item/1dab3646-e2da-01e7-2c3a-1a294xa092aw",
        "item/dasddadd-e2da-01e7-2c3a-123ewdfsa2r5",
        "item/dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf",
        "item/1dab3646-e2da-01e7-2c3a-1a294xa092aw",
        "item/dasddadd-e2da-01e7-2c3a-123ewdfsa2r5",
        "item/dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf"
      ]
    }
    """
    Then the response should be empty
    And the response status code should be 201

    When I send a "GET" request to "/user/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
