Feature: Handle item via the RESTful API

  In order to sell items
  As a software owner
  I need to able to let management users

  Background:
    Given I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"

  @createSchema @dropSchema
  Scenario: get users
    Given there are Users as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with the following details:
      | username  | email          | password |
      | jbravo    | johny@bravo.io | hashed   |
    And the User details are as following:
      | nick | Johny Bravo |
      | type | user |
      | active | 1 |
      | about | I am a hardcore player! |
      | reputation | 21000 |
      | timezone | UTC |
      | boosts | 25200 |
    And the User has Address as following:
      | street                | city | country |
      | 7th & 45th, Manhattan | NYC  | USA     |
    And the User has interests as following:
      | name       |
      | Fishing    |
      | Bowling    |
      | Dodgeball  |
    And the User has Gzr as following:
      | type   | id                                   | amount |
      | wallet | 1dab3646-e2da-01e7-2c3a-1a294xa092aw | 130    |
    And the User has Avatar as following:
      | type | uri                                             |
      | 2d   | http://s3.amazonaws.com/gizer/avatars/clown.jpg |
    And the User has Social as following:
      | name     | url                                          |
      | google+  | https://plus.google.com/11730245375828271109 |
      | facebook | https://www.facebook.com/jbravo              |
    And the User knows:
      | id                                   |
      | 1dab3646-e2da-01e7-2c3a-1a294xa092aw |
      | 1dab3646-e2da-01e7-2c3a-123ewdfsa2r5 |
      | dasddadd-e2da-01e7-2c3a-123ewdfsa2r5 |
      | dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf |
      | zcnw2ss2-dsdd-ssds-d332-321asdasfs2x |
    And the User owns:
      | id                                        |
      | item/1dab3646-e2da-01e7-2c3a-1a294xa092aw |
      | item/dasddadd-e2da-01e7-2c3a-123ewdfsa2r5 |
      | item/dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf |

    When I send a "GET" request to "/user"
    Then the JSON should match pattern:
    """
    [{
        "id": "@string@",
        "nick": "Johny Bravo",
        "email": "johny@bravo.io",
        "type": "user",
        "username": "jbravo",
        "active": 1,
        "address":{
            "street":"7th & 45th, Manhattan",
            "city":"NYC",
            "country":"USA"
        },
        "about":"I am a hardcore player!",
        "reputation": 21000,
        "timezone":"UTC",
        "interests":[
            "Fishing",
            "Bowling",
            "Dodgeball"
        ],
        "gzr":{
            "type":"wallet",
            "id":"1dab3646-e2da-01e7-2c3a-1a294xa092aw",
            "amount":130
        },
        "boosts":25200,
        "avatar":{
            "type":"2d",
            "uri":"http://s3.amazonaws.com/gizer/avatars/clown.jpg"
        },
        "social":[
            {
                "name":"google+",
                "url":"https://plus.google.com/11730245375828271109"
            },
            {
                "name":"facebook",
                "url":"https://www.facebook.com/jbravo"
            }
        ],
        "knows":[
            "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
            "1dab3646-e2da-01e7-2c3a-123ewdfsa2r5",
            "dasddadd-e2da-01e7-2c3a-123ewdfsa2r5",
            "dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf",
            "zcnw2ss2-dsdd-ssds-d332-321asdasfs2x"
        ],
        "owns": [
            "item/1dab3646-e2da-01e7-2c3a-1a294xa092aw",
            "item/dasddadd-e2da-01e7-2c3a-123ewdfsa2r5",
            "item/dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf"
        ],
        "transactions": [],
        "created_at": "@string@.isDateTime()"
    }]
    """

  @createSchema @dropSchema
  Scenario: get user - search by wallet ID
    Given there are Users as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with the following details:
      | username  | email          | password |
      | jbravo    | johny@bravo.io | hashed   |
    And the User details are as following:
      | nick | Johny Bravo |
      | type | user |
      | active | 1 |
      | about | I am a hardcore player! |
      | reputation | 21000 |
      | timezone | UTC |
      | boosts | 25200 |
    And the User has Address as following:
      | street                | city | country |
      | 7th & 45th, Manhattan | NYC  | USA     |
    And the User has interests as following:
      | name       |
      | Fishing    |
      | Bowling    |
      | Dodgeball  |
    And the User has Gzr as following:
      | type   | id                                        | amount |
      | wallet | 0x0f56E808AF5E144d548692550968d800cDe2AcF | 130    |
    And the User has Avatar as following:
      | type | uri                                             |
      | 2d   | http://s3.amazonaws.com/gizer/avatars/clown.jpg |
    And the User has Social as following:
      | name     | url                                          |
      | google+  | https://plus.google.com/11730245375828271109 |
      | facebook | https://www.facebook.com/jbravo              |
    And the User knows:
      | id                                   |
      | 1dab3646-e2da-01e7-2c3a-1a294xa092aw |
      | 1dab3646-e2da-01e7-2c3a-123ewdfsa2r5 |
      | dasddadd-e2da-01e7-2c3a-123ewdfsa2r5 |
      | dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf |
      | zcnw2ss2-dsdd-ssds-d332-321asdasfs2x |
    And the User owns:
      | id                                        |
      | item/1dab3646-e2da-01e7-2c3a-1a294xa092aw |
      | item/dasddadd-e2da-01e7-2c3a-123ewdfsa2r5 |
      | item/dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf |

    When I send a "GET" request to "/user?wallet=0x0f56E808AF5E144d548692550968d800cDe2AcF"
    Then the JSON should match pattern:
    """
    [{
        "id": "@string@",
        "nick": "Johny Bravo",
        "email": "johny@bravo.io",
        "type": "user",
        "username": "jbravo",
        "active": 1,
        "address": {
            "street": "7th & 45th, Manhattan",
            "city": "NYC",
            "country": "USA"
        },
        "about": "I am a hardcore player!",
        "reputation": 21000,
        "timezone": "UTC",
        "interests": [
            "Fishing",
            "Bowling",
            "Dodgeball"
        ],
        "gzr": {
            "type": "wallet",
            "id": "0x0f56E808AF5E144d548692550968d800cDe2AcF",
            "amount":130
        },
        "boosts": 25200,
        "avatar": {
            "type": "2d",
            "uri": "http://s3.amazonaws.com/gizer/avatars/clown.jpg"
        },
        "social": [
            {
                "name": "google+",
                "url": "https://plus.google.com/11730245375828271109"
            },
            {
                "name": "facebook",
                "url": "https://www.facebook.com/jbravo"
            }
        ],
        "knows": [
            "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
            "1dab3646-e2da-01e7-2c3a-123ewdfsa2r5",
            "dasddadd-e2da-01e7-2c3a-123ewdfsa2r5",
            "dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf",
            "zcnw2ss2-dsdd-ssds-d332-321asdasfs2x"
        ],
        "owns": [
            "item/1dab3646-e2da-01e7-2c3a-1a294xa092aw",
            "item/dasddadd-e2da-01e7-2c3a-123ewdfsa2r5",
            "item/dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf"
        ],
        "transactions": [],
        "created_at": "@string@.isDateTime()"
    }]
    """

  @createSchema @dropSchema
  Scenario: get user - search by wallet ID - wrong wallet
    Given there are Users as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with the following details:
      | username  | email          | password |
      | jbravo    | johny@bravo.io | hashed   |
    And the User details are as following:
      | nick | Johny Bravo |
      | type | user |
      | active | 1 |
      | about | I am a hardcore player! |
      | reputation | 21000 |
      | timezone | UTC |
      | boosts | 25200 |
    And the User has Address as following:
      | street                | city | country |
      | 7th & 45th, Manhattan | NYC  | USA     |
    And the User has interests as following:
      | name       |
      | Fishing    |
      | Bowling    |
      | Dodgeball  |
    And the User has Gzr as following:
      | type   | id                                        | amount |
      | wallet | 0x0f56E808AF5E144d548692550968d800cDe2AcF | 130    |
    And the User has Avatar as following:
      | type | uri                                             |
      | 2d   | http://s3.amazonaws.com/gizer/avatars/clown.jpg |
    And the User has Social as following:
      | name     | url                                          |
      | google+  | https://plus.google.com/11730245375828271109 |
      | facebook | https://www.facebook.com/jbravo              |
    And the User knows:
      | id                                   |
      | 1dab3646-e2da-01e7-2c3a-1a294xa092aw |
      | 1dab3646-e2da-01e7-2c3a-123ewdfsa2r5 |
      | dasddadd-e2da-01e7-2c3a-123ewdfsa2r5 |
      | dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf |
      | zcnw2ss2-dsdd-ssds-d332-321asdasfs2x |
    And the User owns:
      | id                                        |
      | item/1dab3646-e2da-01e7-2c3a-1a294xa092aw |
      | item/dasddadd-e2da-01e7-2c3a-123ewdfsa2r5 |
      | item/dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf |

    When I send a "GET" request to "/user?wallet=qwerty"
    Then the JSON should match pattern:
    """
    []
    """