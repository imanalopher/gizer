Feature: Handle item via the RESTful API

  In order to sell items
  As a software owner
  I need to able to let management users

  Background:
    Given I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"

  @createSchema @dropSchema
  Scenario: update user - add transaction
    Given there are Users as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with the following details:
      | username  | email          | password |
      | jbravo    | johny@bravo.io | hashed   |
    And the User details are as following:
      | nick | Johny Bravo |
      | type | user |
      | active | 1 |
      | about | I am a hardcore player! |
      | reputation | 21000 |
      | timezone | UTC |
      | boosts | 25200 |
    And the User has Address as following:
      | street                | city | country |
      | 7th & 45th, Manhattan | NYC  | USA     |
    And the User has interests as following:
      | name       |
      | Fishing    |
      | Bowling    |
      | Dodgeball  |
    And the User has Gzr as following:
      | type   | id                                   | amount |
      | wallet | 1dab3646-e2da-01e7-2c3a-1a294xa092aw | 130    |
    And the User has Avatar as following:
      | type | uri                                             |
      | 2d   | http://s3.amazonaws.com/gizer/avatars/clown.jpg |
      And the User has Social as following:
      | name     | url                                          |
      | google+  | https://plus.google.com/11730245375828271109 |
      | facebook | https://www.facebook.com/jbravo              |
      And the User has Transactions as following:
      | txId                                 | eth | gzr  | confirmedAt               |
      | f309ac7a-ab8b-40f1-9595-43837c497f6f | 2   | 8000 | 2018-01-01T01:10:00+01:00 |
    And the User knows:
      | id                                   |
      | 1dab3646-e2da-01e7-2c3a-1a294xa092aw |
      | 1dab3646-e2da-01e7-2c3a-123ewdfsa2r5 |
      | dasddadd-e2da-01e7-2c3a-123ewdfsa2r5 |
      | dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf |
      | zcnw2ss2-dsdd-ssds-d332-321asdasfs2x |
    And the User owns:
      | id                                        |
      | item/1dab3646-e2da-01e7-2c3a-1a294xa092aw |
      | item/dasddadd-e2da-01e7-2c3a-123ewdfsa2r5 |
      | item/dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf |

    When I send a "PATCH" request to "/user/4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with body:
    """
    {
        "transactions": [
          {
            "tx_id": "98f91a8b-96ec-4ec0-80e3-1f193d81d56f",
            "eth": 123.123456789,
            "gzr": 2000,
            "confirmed_at": "2018-03-02T01:10:00+01:00"
          },
          {
            "tx_id": "385dde6a-798b-403b-821b-bfcb0b39d6f6",
            "eth": 0.50,
            "gzr": 2000,
            "confirmed_at": "2018-03-01T01:10:00+01:00"
          },
          {
            "tx_id": "8061d495-dfb5-4443-9955-83e12337065a",
            "eth": 3,
            "gzr": 12000,
            "confirmed_at": "2018-02-28T01:10:00+01:00"
          }
        ]
    }
    """
    Then the response should be empty
    And the response status code should be 201

    When I send a "GET" request to "/user/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
        "id": "@string@",
        "nick": "Johny Bravo",
        "email": "johny@bravo.io",
        "type": "user",
        "username": "jbravo",
        "active": 1,
        "address": {
            "street": "7th & 45th, Manhattan",
            "city": "NYC",
            "country": "USA"
        },
        "about": "I am a hardcore player!",
        "reputation": 21000,
        "timezone": "UTC",
        "interests":[
            "Fishing",
            "Bowling",
            "Dodgeball"
        ],
        "gzr": {
            "type": "wallet",
            "id": "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
            "amount":130
        },
        "boosts":25200,
        "avatar": {
            "type": "2d",
            "uri": "http://s3.amazonaws.com/gizer/avatars/clown.jpg"
        },
        "social":[
            {
                "name": "google+",
                "url": "https://plus.google.com/11730245375828271109"
            },
            {
                "name": "facebook",
                "url": "https://www.facebook.com/jbravo"
            }
        ],
        "knows":[
            "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
            "1dab3646-e2da-01e7-2c3a-123ewdfsa2r5",
            "dasddadd-e2da-01e7-2c3a-123ewdfsa2r5",
            "dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf",
            "zcnw2ss2-dsdd-ssds-d332-321asdasfs2x"
        ],
        "owns": [
            "item/1dab3646-e2da-01e7-2c3a-1a294xa092aw",
            "item/dasddadd-e2da-01e7-2c3a-123ewdfsa2r5",
            "item/dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf"
        ],
        "transactions": [
          {
            "tx_id": "98f91a8b-96ec-4ec0-80e3-1f193d81d56f",
            "eth": 123.123456789,
            "gzr": 2000,
            "confirmed_at": "2018-03-02T01:10:00+01:00"
          },
          {
            "tx_id": "385dde6a-798b-403b-821b-bfcb0b39d6f6",
            "eth": 0.50,
            "gzr": 2000,
            "confirmed_at": "2018-03-01T01:10:00+01:00"
          },
          {
            "tx_id": "8061d495-dfb5-4443-9955-83e12337065a",
            "eth": 3,
            "gzr": 12000,
            "confirmed_at": "2018-02-28T01:10:00+01:00"
          },
          {
            "tx_id": "f309ac7a-ab8b-40f1-9595-43837c497f6f",
            "eth": 2,
            "gzr": 8000,
            "confirmed_at": "2018-01-01T01:10:00+01:00"
          }
        ],
        "created_at": "@string@.isDateTime()"
    }
    """

  @createSchema @dropSchema
  Scenario: update user - send empty transaction
    Given there are Users as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with the following details:
      | username  | email          | password |
      | jbravo    | johny@bravo.io | hashed   |
    And the User details are as following:
      | nick | Johny Bravo |
      | type | user |
      | active | 1 |
      | about | I am a hardcore player! |
      | reputation | 21000 |
      | timezone | UTC |
      | boosts | 25200 |
    And the User has Address as following:
      | street                | city | country |
      | 7th & 45th, Manhattan | NYC  | USA     |
    And the User has interests as following:
      | name       |
      | Fishing    |
      | Bowling    |
      | Dodgeball  |
    And the User has Gzr as following:
      | type   | id                                   | amount |
      | wallet | 1dab3646-e2da-01e7-2c3a-1a294xa092aw | 130    |
    And the User has Avatar as following:
      | type | uri                                             |
      | 2d   | http://s3.amazonaws.com/gizer/avatars/clown.jpg |
      And the User has Social as following:
      | name     | url                                          |
      | google+  | https://plus.google.com/11730245375828271109 |
      | facebook | https://www.facebook.com/jbravo              |
      And the User has Transactions as following:
      | txId                                 | eth | gzr  | confirmedAt               |
      | f309ac7a-ab8b-40f1-9595-43837c497f6f | 2   | 8000 | 2018-01-01T01:10:00+01:00 |
    And the User knows:
      | id                                   |
      | 1dab3646-e2da-01e7-2c3a-1a294xa092aw |
      | 1dab3646-e2da-01e7-2c3a-123ewdfsa2r5 |
      | dasddadd-e2da-01e7-2c3a-123ewdfsa2r5 |
      | dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf |
      | zcnw2ss2-dsdd-ssds-d332-321asdasfs2x |
    And the User owns:
      | id                                        |
      | item/1dab3646-e2da-01e7-2c3a-1a294xa092aw |
      | item/dasddadd-e2da-01e7-2c3a-123ewdfsa2r5 |
      | item/dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf |

    When I send a "PATCH" request to "/user/4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with body:
    """
    {
        "transactions": []
    }
    """
    And the response should be empty
    And the response status code should be 201

    When I send a "GET" request to "/user/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the JSON should match pattern:
    """
    {
        "id": "@string@",
        "nick": "Johny Bravo",
        "email": "johny@bravo.io",
        "type": "user",
        "username": "jbravo",
        "active": 1,
        "address": {
            "street": "7th & 45th, Manhattan",
            "city": "NYC",
            "country": "USA"
        },
        "about": "I am a hardcore player!",
        "reputation": 21000,
        "timezone": "UTC",
        "interests":[
            "Fishing",
            "Bowling",
            "Dodgeball"
        ],
        "gzr": {
            "type": "wallet",
            "id": "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
            "amount":130
        },
        "boosts":25200,
        "avatar": {
            "type": "2d",
            "uri": "http://s3.amazonaws.com/gizer/avatars/clown.jpg"
        },
        "social":[
            {
                "name": "google+",
                "url": "https://plus.google.com/11730245375828271109"
            },
            {
                "name": "facebook",
                "url": "https://www.facebook.com/jbravo"
            }
        ],
        "knows":[
            "1dab3646-e2da-01e7-2c3a-1a294xa092aw",
            "1dab3646-e2da-01e7-2c3a-123ewdfsa2r5",
            "dasddadd-e2da-01e7-2c3a-123ewdfsa2r5",
            "dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf",
            "zcnw2ss2-dsdd-ssds-d332-321asdasfs2x"
        ],
        "owns": [
            "item/1dab3646-e2da-01e7-2c3a-1a294xa092aw",
            "item/dasddadd-e2da-01e7-2c3a-123ewdfsa2r5",
            "item/dasddadd-dsdd-ssds-d332-sdfs3q3sdfaf"
        ],
        "transactions": [
          {
            "tx_id": "f309ac7a-ab8b-40f1-9595-43837c497f6f",
            "eth": 2,
            "gzr": 8000,
            "confirmed_at": "2018-01-01T01:10:00+01:00"
          }
        ],
        "created_at": "@string@.isDateTime()"
    }
    """