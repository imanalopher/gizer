Feature: Handle item via the RESTful API
  In order to get items
  As a player
  I need to able to open chest

  Background:
    Given I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"

  @createSchema @dropSchema
  Scenario: update chest
    Given there are Chest as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    And there are Users as "239ds9af-ape3-a033k-pwo9-qwvshe309swo" with the following details:
      | username   | email           | password |
      | jbravo2    | johny@bravo2.io | hashed   |
    And the Chest details are as following:
      | name       | chest name      |
      | collection | collection name |
      | price      | 1               |
      | status     | locked          |
    And the Chest Items as following:
      | id                                   |
      | 2adc3646-1bda-a1e7-8c3f-9a214cf093ae |
    And there are Item as "8d5f9ace-c4c3-4799-a661-164b3ffc3a66"

    When I send a PATCH request to "/chest/4ddb3646-ebda-11e7-8c3f-9a214cf093ae" with body:
    """
    {
        "status": "pending",
        "user": "239ds9af-ape3-a033k-pwo9-qwvshe309swo",
        "items": [
            "2adc3646-1bda-a1e7-8c3f-9a214cf093ae",
            "8d5f9ace-c4c3-4799-a661-164b3ffc3a66"
        ],
        "transaction_id": "01a5d507-fe1b-4cbb-928c-e9a6eb683c00"
    }
    """
    Then the response status code should be 201
    And the response should be empty

    When I send a GET request to "/chest/4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    Then the response status code should be 200
    And the JSON should match pattern:
    """
    {
        "id": "4ddb3646-ebda-11e7-8c3f-9a214cf093ae",
        "name": "chest name",
        "collection": "collection name",
        "price": 1,
        "status": "pending",
        "user": "239ds9af-ape3-a033k-pwo9-qwvshe309swo",
        "items": [
            "2adc3646-1bda-a1e7-8c3f-9a214cf093ae",
            "8d5f9ace-c4c3-4799-a661-164b3ffc3a66"
        ],
        "transaction_id": "01a5d507-fe1b-4cbb-928c-e9a6eb683c00",
        "created_at": "@string@.isDateTime()"
    }
    """

    Given there are Chest as "0d977a15-6740-4221-9571-afee4368767c"
    And the Chest details are as following:
      | name       | chest name 2      |
      | collection | collection name 2 |
      | price      | 1                 |
      | status     | locked            |
    When I send a PATCH request to "/chest/0d977a15-6740-4221-9571-afee4368767c" with body:
    """
    {
        "status": "pending",
        "user": "239ds9af-ape3-a033k-pwo9-qwvshe309swo",
        "items": [
            "8d5f9ace-c4c3-4799-a661-164b3ffc3a66"
        ],
        "transaction_id": "4a6b0d91-b7d4-4c7d-a372-3c9b0d33486e"
    }
    """
    Then the response status code should be 201
    And the response should be empty

    When I send a GET request to "/chest/0d977a15-6740-4221-9571-afee4368767c"
    Then the response status code should be 200
    And the JSON should match pattern:
    """
    {
        "id": "0d977a15-6740-4221-9571-afee4368767c",
        "name": "chest name 2",
        "collection": "collection name 2",
        "price": 1,
        "status": "pending",
        "user": "239ds9af-ape3-a033k-pwo9-qwvshe309swo",
        "items": [
            "8d5f9ace-c4c3-4799-a661-164b3ffc3a66"
        ],
        "transaction_id": "4a6b0d91-b7d4-4c7d-a372-3c9b0d33486e",
        "created_at": "@string@.isDateTime()"
    }
    """