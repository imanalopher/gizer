Feature: Handle item via the RESTful API
  In order to get items
  As a administrator
  I need to able to managment chest

  Background:
    Given I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"

  @createSchema @dropSchema
  Scenario: create chest
    Given there are Item as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"

    When I send a PUT request to "/chest" with body:
    """
    {
        "name": "chest name",
        "collection": "collection name",
        "price": 1,
        "transaction_id": "0x0f56E808AF5E144d548692550968d800cDe2AcF"
    }
    """
    Then the response status code should be 201
    And the JSON should match pattern:
    """
    {
        "id": "@string@",
        "status": "locked"
    }
    """