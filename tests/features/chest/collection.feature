Feature: Handle item via the RESTful API
  In order to get items
  As a player
  I need to able to open chest

  Background:
    Given I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"

  @createSchema @dropSchema
  Scenario: get chest
    Given there are Chest as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    And the Chest details are as following:
      | name | chest name |
      | collection | collection name |
      | price | 1 |
      | status | locked |
    And the Chest Items as following:
      | id                                   |
      | 2adc3646-1bda-a1e7-8c3f-9a214cf093ae |
    And the Chest owner is "239ds9af-ape3-a033k-pwo9-qwvshe309swo"

    When I send a GET request to "/chests"
    Then the response status code should be 200
    And the JSON should match pattern:
    """
    [
      {
          "id": "@string@",
          "name": "chest name",
          "collection": "collection name",
          "price": 1,
          "status": "locked",
          "user": "@string@",
          "items": [
              "@string@"
          ],
          "created_at": "@string@.isDateTime()"
      }
    ]
    """