Feature: Handle item via the RESTful API
  In order to get items
  As a player
  I need to able to open chest

  Background:
    Given I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"

  @createSchema @dropSchema
  Scenario: extract chest
    Given there are Chest as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    And the Chest details are as following:
      | name | chest name |
      | collection | collection name |
      | price | 1 |
      | status | locked |
    And the Chest Items as following:
      | id                                   |
      | 2adc3646-1bda-a1e7-8c3f-9a214cf093ae |
    And the Chest owner is "239ds9af-ape3-a033k-pwo9-qwvshe309swo"

    When I send a GET request to "/chest"
    Then the response status code should be 200
    And the JSON should match pattern:
    """
    {
        "id": "@string@",
        "name": "chest name",
        "collection": "collection name",
        "price": 1
    }
    """

  @createSchema @dropSchema
  Scenario: extract chest - locked not available
    Given there are Chest as "4ddb3646-ebda-11e7-8c3f-9a214cf093ae"
    And the Chest details are as following:
      | name | chest name |
      | collection | collection name |
      | price | 1 |
      | status | unlocked |
    And the Chest Items as following:
      | id                                   |
      | 2adc3646-1bda-a1e7-8c3f-9a214cf093ae |
    And the Chest owner is "239ds9af-ape3-a033k-pwo9-qwvshe309swo"

    And there are Chest as "psj72nas-ebda-11e7-8c3f-9a214cf093ae"
    And the Chest details are as following:
      | name | chest 2 name |
      | collection | collection name |
      | price | 2 |
      | status | pending |
    And the Chest Items as following:
      | id                                   |
      | 2adc3646-1bda-a1e7-8c3f-9a214cf093ae |
    And the Chest owner is "l3a96d09-ape3-a033k-pwo9-qwvshe309swo"

    When I send a GET request to "/chest"
    Then the response status code should be 404

  @createSchema @dropSchema
  Scenario: get chest info
    Given there are Chest as "clap0s72-ebda-11e7-8c3f-9a214cf093ae"
    And the Chest details are as following:
      | name | chest name |
      | collection | collection name |
      | price | 1 |
      | status | unlocked |
    And the Chest Items as following:
      | id                                   |
      | 2adc3646-1bda-a1e7-8c3f-9a214cf093ae |
    And the Chest owner is "239ds9af-ape3-a033k-pwo9-qwvshe309swo"

    When I send a GET request to "/chest/clap0s72-ebda-11e7-8c3f-9a214cf093ae"
    Then the response status code should be 200
    And the JSON should match pattern:
    """
    {
        "id": "@string@",
        "name": "chest name",
        "collection": "collection name",
        "price": 1,
        "status": "unlocked",
        "user": "@string@",
        "items": [
            "@string@"
        ],
        "transaction_id": "@null@",
        "created_at": "@string@.isDateTime()"
    }
    """
